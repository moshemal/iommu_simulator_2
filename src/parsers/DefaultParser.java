package parsers;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;

import visitor_pattern.*;
/*
 * this parser go with 2_10 trace files
 */

public class DefaultParser implements IParser{
	
	final int dontCare = -1;
	final String filePath;
	FileInputStream fstream;
	DataInputStream in;
	BufferedReader reader;
	HashMap<Integer,Integer> busDevFub2Domain = new HashMap<Integer,Integer>(); //TODO: move it to the constructor
	
	public DefaultParser(String filePath_) {
		initBusDevFun2Domain();
		filePath = filePath_;
		try {
			fstream = new FileInputStream(filePath);
			in = new DataInputStream(fstream);
			reader = new BufferedReader(new InputStreamReader(in));
		} catch (FileNotFoundException e) {
			System.err.println("File input error");
			e.printStackTrace();
		}
	}
	
	/*	Read: 
	 *  read 9 fffff000 24634000
	 *	Write:
	 *  write 9 ffffd000 32059000
	 */	 
	public EventElement getEvent(){
		
		try {
			String line;
			if((line = reader.readLine()) == null)
				return null;
			String[] tokens = line.split(" ");
			
			if ( (tokens[0].compareTo("read") == 0) || (tokens[0].compareTo("write") == 0) )
				return creatReadWrite(tokens);
			 
			
			if ((tokens[0].compareTo("reg") == 0) && (tokens[1].compareTo("write") == 0))
				return mappedIoWrite(tokens);
			
			
		} catch (IOException e) {
			System.err.println("getEvent split line error");
			e.printStackTrace();
		}
		return new NopEvent();
	}
	
	public Collection<Integer> getDomainIds(){
		return this.busDevFub2Domain.values();
	}
		
	private EventElement mappedIoWrite(String[] tokens){
		if (tokens[5].compareTo("c0") == 0) {
			return creatMapUnmap(tokens);
		}
		return new NopEvent();
	}
	
	/*  Unmap:
	 *	reg write ffffd000 to address c0
	 *	reg write 0 to address c4
	 *	reg write 1 to address c8
		reg write b0000002 to address cc
		Map:
		reg write ffffd000 to address c0
		reg write 0 to address c4
		reg write 2 to address c8
	 *	reg write b0000002 to address cc
	 */
	private EventElement creatMapUnmap(String[] tokens){
		
		String line;
		long numbers[] = new long[4];
		int i = 1;
		int offset = 0xc4;
		
		numbers[0] = Long.parseLong(tokens[2],16);
		try {
			
			while(i<=3 && ((line = reader.readLine()) != null)){
				tokens = line.split(" ");
				
				if (!checkLine(tokens,offset)){
					System.err.println("creatMapUnmap error");
					return null;
				}
				numbers[i++] = Long.parseLong(tokens[2],16);
				offset+=4;
			}
			long msbAddr = numbers[1];
			msbAddr = msbAddr << 32;
			long vAddr = numbers[0] |  msbAddr;
			
			EventElement event;
			if((event = getSpecificMapUnmap((int)(0xFFF & numbers[3]), vAddr,0,numbers[2])) != null)
				return event;
			
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//shouldn't get here
		System.err.println("creatMapUnmap error");
		return null;
	}
	
	private boolean checkLine(String[] tokens, int offset){
		if ((tokens == null) ||
			(tokens[0].compareTo("reg")!=0) ||
			(tokens[1].compareTo("write")!=0) ||
			(Integer.parseInt(tokens[5],16) != offset))
				return false;
		return true;
	}
	private EventElement creatReadWrite(String[] tokens){
		int busDevFun;
		long pAdd, vAdd;
		
		busDevFun	= Integer.parseInt(tokens[1],16);
		vAdd		= Long.parseLong(tokens[2],16);
		pAdd		= Long.parseLong(tokens[3],16);
		
		if(tokens[0].compareTo("read") == 0)
			return new ReadEvent(busDevFun2Domain(busDevFun), vAdd, pAdd);
		if(tokens[0].compareTo("write") == 0)
			return new WriteEvent(busDevFun2Domain(busDevFun), vAdd, pAdd);
		
		//shouldn't get here
		System.err.println("DefaultParser.creatReadWrite error");
		return null;
	}
	
	
	private int busDevFun2Domain(int busDevFun){
		Integer domain;
		domain = this.busDevFub2Domain.get(busDevFun);
		if (domain != null)
			return domain.intValue();
		return -1;
	}
	/*
	 * this functions must be changed while using different trace file
	 */
	private void initBusDevFun2Domain(){
		this.busDevFub2Domain.put(0x9, 2);
		this.busDevFub2Domain.put(0x18, 3);
	}
	
	/*
	 * 4 or 1 UnMap
	 * 5 or 2 Map
	 * */
	/*private IEvent getSpecificMapUnmap_(int domain, long virtualAdd, long physicalAdd, long value){
		if(value == 1) //4 or 1
			return new UnMapEvent(domain, virtualAdd,physicalAdd);
		if(value == 2) // 5 or 2
			return new MapEvent(domain, virtualAdd,physicalAdd);
		return null;
	}*/
	
	

	/*
		MOSHE_ALLOC_COHERENT = 11,
		MOSHE_FREE_COHERENT	= 12,
		MOSHE_MAP_SG		= 13,
		MOSHE_UNMAP_SG		= 14,
		MOSHE_MAP_PAGE		= 15,
		MOSHE_UNMAP_PAGE	= 16,
		MOSHE_UNKNOWN_MAP	= 33,
		MOSHE_UNKNOWN_UNMAP	= 34,
		MOSHE_UNKNOWN		= 35,
	*/
	private EventElement getSpecificMapUnmap(int domain, long virtualAdd, long physicalAdd, long value){
		
		switch ((int)value){
		case 11: return new MapCoherentEvent(0, domain,virtualAdd);
		case 12: return new UnMapCoherentEvent(0, domain,virtualAdd);
		case 13: return new MapSgEvent(dontCare, domain,virtualAdd);
		case 14: return new UnMapSgEvent(dontCare, domain,virtualAdd);
		case 15: return new MapEvent(dontCare, domain,virtualAdd);
		case 16: return new UnMapEvent(dontCare, domain,virtualAdd);
		case 33: return null;//new MapEvent(domain, virtualAdd,physicalAdd);
		case 34: return null;//new UnMapEvent(domain, virtualAdd,physicalAdd);
		default: return null;
		}
	}
		
}
