package parsers;

import java.util.Collection;

import visitor_pattern.EventElement;

public interface IParser {
	public EventElement getEvent();
	public Collection<Integer> getDomainIds();
}
