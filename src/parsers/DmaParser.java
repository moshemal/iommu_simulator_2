package parsers;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import visitor_pattern.*;

public class DmaParser implements IParser{
	
	final String filePath;
	FileInputStream fstream;
	DataInputStream in;
	BufferedReader reader;
	ReadEvent lastReadEvent = null;
	String lastLine			= null;
	HashMap<Integer,Integer> busDevFun2Domain = new HashMap<Integer,Integer>(); //TODO: move it to the constructor
	
	public DmaParser(String filePath_) {
		initBusDevFun2Domain();
		filePath = filePath_;
		try {
			fstream = new FileInputStream(filePath);
			in = new DataInputStream(fstream);
			reader = new BufferedReader(new InputStreamReader(in));
		} catch (FileNotFoundException e) {
			System.err.println("File input error");
			e.printStackTrace();	
		}
	}
	
	/*	Read: 
	 *  read 9 fffff000 24634000
	 *	Write:
	 *  write 9 ffffd000 32059000
	 */	 
	public EventElement getEvent(){
		
		try {
			String line;
			if(lastLine != null){
				line = lastLine;
				lastLine = null;
			}else{
				line = reader.readLine();
			}
			if(line == null)
				return null;
			String[] tokens = line.split(" ");
			
			//to evaluate the hint the QEMU execute dma_read from the ring descriptors 
			// so we ignore the last read - it is a fake one
			if (tokens[0].compareTo("hint") == 0){
				lastReadEvent = null;
				return createHint(tokens); 
			}
			if (lastReadEvent != null){
				lastLine = line;
				ReadEvent res = lastReadEvent;
				lastReadEvent = null;
				return res;
			}
			
			if ( (tokens[0].compareTo("read") == 0) || (tokens[0].compareTo("write") == 0) )
				return creatReadWrite(tokens);
			
			
			
			if ((tokens[0].compareTo("reg") == 0) && (tokens[1].compareTo("write") == 0))
				return mappedIoWrite(tokens);
			
			
		} catch (IOException e) {
			System.err.println("getEvent split line error");
			e.printStackTrace();
		}
		return new NopEvent();
	}

	public Collection<Integer> getDomainIds(){
		return this.busDevFun2Domain.values();
	}
		
	private EventElement mappedIoWrite(String[] tokens){
		if (tokens[5].compareTo("c0") == 0) {
			return creatMapUnmap(tokens);
		}
		return new NopEvent();
	}
	
	/*  Unmap:
	 *	reg write ffffd000 to address c0
	 *	reg write 0 to address c4
	 *	reg write 1 to address c8
		reg write b0000002 to address cc
		Map:
		reg write ffffd000 to address c0
		reg write 0 to address c4
		reg write 2 to address c8
	 *	reg write b0000002 to address cc
	 */
	private EventElement creatMapUnmap(String[] tokens){
		
		String line;
		long numbers[] = new long[4];
		int i = 1;
		int offset = 0xc4;
		
		numbers[0] = Long.parseLong(tokens[2],16);
		try {
			
			while(i<=3 && ((line = reader.readLine()) != null)){
				tokens = line.split(" ");
				
				if (!checkLine(tokens,offset)){
					System.err.println("creatMapUnmap error");
					return null;
				}
				numbers[i++] = Long.parseLong(tokens[2],16);
				offset+=4;
			}
			long msbAddr = numbers[1];
			msbAddr = msbAddr << 32;
			long vAddr = numbers[0] |  msbAddr;
			
			EventElement event;
			if((event = getSpecificMapUnmap((int)(0xFFF & numbers[3]), vAddr,0,numbers[2])) != null)
				return event;
			
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//shouldn't get here
		System.err.println("creatMapUnmap error");
		return null;
	}
	
	private boolean checkLine(String[] tokens, int offset){
		if ((tokens == null) ||
			(tokens[0].compareTo("reg")!=0) ||
			(tokens[1].compareTo("write")!=0) ||
			(Integer.parseInt(tokens[5],16) != offset))
				return false;
		return true;
	}
	private EventElement creatReadWrite(String[] tokens){
		int busDevFun;
		long pAdd, vAdd;
		
		busDevFun	= Integer.parseInt(tokens[1],16);
		vAdd		= Long.parseLong(tokens[2],16);
		pAdd		= Long.parseLong(tokens[3],16);
		
		if(tokens[0].compareTo("read") == 0){
			lastReadEvent = new ReadEvent(busDevFun2Domain(busDevFun), vAdd, pAdd);
			return new NopEvent();
		}
		if(tokens[0].compareTo("write") == 0)
			return new WriteEvent(busDevFun2Domain(busDevFun), vAdd, pAdd);
		
		//shouldn't get here
		System.err.println("DefaultParser.creatReadWrite error");
		return null;
	}
	
	
	private int busDevFun2Domain(int busDevFun){
		Integer domain;
		domain = this.busDevFun2Domain.get(busDevFun);
		if (domain != null)
			return domain.intValue();
		return -1;
	}
	/*
	 * this functions must be changed while using different trace file
	 */
	private void initBusDevFun2Domain(){
		this.busDevFun2Domain.put(0x9, 2);
		this.busDevFun2Domain.put(0x18, 3);
	}
	
	

	/*
		MOSHE_ALLOC_COHERENT = 11,
		MOSHE_FREE_COHERENT	= 12,
		MOSHE_MAP_SG		= 13,
		MOSHE_UNMAP_SG		= 14,
		MOSHE_MAP_PAGE		= 15,
		MOSHE_UNMAP_PAGE	= 16,
		MOSHE_UNKNOWN_MAP	= 33,
		MOSHE_UNKNOWN_UNMAP	= 34,
		MOSHE_UNKNOWN		= 35,
	*/
	private EventElement getSpecificMapUnmap(int domain, long virtualAdd, long physicalAdd, long value){
		
		int direction = (int)value / 100;
		int type = (int)value % 100;
		
		switch (type){
		case 11: return new MapCoherentEvent(direction, domain, virtualAdd);
		case 12: return new UnMapCoherentEvent(direction, domain, virtualAdd);
		case 13: return new MapSgEvent(direction, domain, virtualAdd);
		case 14: return new UnMapSgEvent(direction, domain, virtualAdd);
		case 15: return new MapEvent(direction, domain, virtualAdd);
		case 16: return new UnMapEvent(direction, domain, virtualAdd);
		case 33: return null;//new MapEvent(domain, virtualAdd,physicalAdd);
		case 34: return null;//new UnMapEvent(domain, virtualAdd,physicalAdd);
		default: return null;
		}
	}
	
	/*
	 * 
	 * hint rx_desc e1000 rdh=126 rdt=124: fff82840 fff7f040 fff7e840 fff81040 fff80840
	 * hint tx_desc e1000: 322200000000 fff87d0a fff83000
	*/
	private EventElement createHint(String[] tokens) {
		
		LinkedList<Long> list = new LinkedList<Long>();
		long address;
		int i, domain=3; //TODO: initialize the domain dinamicly by the name of the device;
		EventElement res = null;
		
		if (tokens[1].compareTo("rx_desc") == 0){
			i = 5;
			res = new HintRxEvent(domain, list);
		}else if (tokens[1].compareTo("tx_desc") == 0){
			i = 3;
			res = new HintTxEvent(domain, list);
		}else{
			return null;
		}
		for (;i<tokens.length;i++){
			address = Long.parseLong(tokens[i],16);
			list.addLast(mask12lsb(address));
		}
		
		return res;
	}
	
	long mask12lsb(long add){
		long mask = 0xFFF;
		long result = (~mask) & add;
		return result;
	}
}
