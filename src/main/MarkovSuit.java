package main;

import simulations.prefetchers.MarkovChainPrefetch;
import simulations.prefetchers.MarkovChainZeroPrefetch;
import visitor_pattern.IEventElementVisitor;

public class MarkovSuit extends PrefetchSuit {

	public MarkovSuit(boolean isZero, ParserType type){
		super(isZero, type);
		header = "#   ----------------------------------------\n" +
				"#   Markov Chain Prefetcher simulation\n" +
				"#   ----------------------------------------\n" +
				"#  running with different sizes of: Prefetch Buffer, TLB, Markov entries\n" +
				"#  those 3 sizes are written above each triple: accuracy efficiency hitRate\n" +
				"#  all markov entries are of size 2\n";
		confTitles = new int[][]{{4,4,8},{4,4,32},{4,4,64},{4,4,128},{4,4,256},{4,4,512}};
				//new int[][]{{4,4,256},{4,32,256},{32,32,256},{4,32,512},{16,32,512},{32,32,512}};
		linesTitles = new String[]{"size of Prefetch Buffer","size of IOTLB","size of Markov Table"};
	}

	@Override
	IEventElementVisitor[] create_sims() {
		
		IEventElementVisitor[] sims = new IEventElementVisitor[confTitles.length];
		if (isZero){
			for (int i=0; i<sims.length; i++)
				sims[i] = new MarkovChainZeroPrefetch(3,confTitles[i][0],confTitles[i][1],confTitles[i][2],2);
		}
		else{
			for (int i=0; i<sims.length; i++)
				sims[i] = new MarkovChainPrefetch(3,confTitles[i][0],confTitles[i][1],confTitles[i][2],2);
		}
		
		return sims;
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (!checkAndParseInOutParam(args))
			return;
		MarkovSuit simulation = new MarkovSuit(isZero(args),getParserType(args));
		
		simulation.print("#    isZero = " + isZero(args) + "\n");
		
		simulation.run(args);

	}

}

//runSimulation(root + "//" + file,new MarkovChainPrefetch(3,4,4,256,2));
////runSimulation(root + "//" + file,new MarkovChainPrefetch(3,4,4,512,2));
////runSimulation(root + "//" + file,new MarkovChainPrefetch(3,4,8,512,2));
////runSimulation(root + "//" + file,new MarkovChainPrefetch(3,8,8,512,2));