package main;

public class RunAll {

	public RunAll() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] arg_zero = {".//..//traces_2_10", "stdout", "default", "zero"};
		String[] arg_no_zero = {".//..//traces_2_10", "stdout", "default", "zero"};
		MarkovSuit.main(arg_zero);
		RecencyBasedSuit.main(arg_zero);
		MarkovSuit.main(arg_no_zero);
		RecencyBasedSuit.main(arg_no_zero);
		DistanceSuit.main(arg_no_zero);

	}

}
