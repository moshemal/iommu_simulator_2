package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import parsers.IParser;

import visitor_pattern.EventElement;
import visitor_pattern.IEventElementVisitor;

public abstract class Main {
	
	String header = "";
	static File inputRootPath;
	static File outputRootPath;
	ParserType parserType = ParserType.DEFAULT;
	static BufferedWriter output; 
	final int startSimNum = 0;
	
	public Main() {}
	
	abstract IEventElementVisitor[] create_sims();
	
	void closeOutput(){
		if (output != null){
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	abstract void printHeader(String[] args);
	
	void run(String[] args){
		printHeader(args);
		runAllFiles(inputRootPath);
		closeOutput();
	}
	
	void print(String str){
		if (output != null)
			try {
				output.write(str);
			} catch (IOException e) {
				e.printStackTrace();
			}
		else System.out.print(str);
	}
	
	void runAllFiles(File root){
		if (!root.isDirectory()){
			if (filter(root.getName()))
				runSimulations(root,create_sims());
			return;
		}
		File[] files = root.listFiles();	
		for(File file : files)
			runAllFiles(file);
	}
	
	void runSimulations(File file,IEventElementVisitor[] sims){
		if (file.isDirectory())
			return;
		IParser parser = this.parserType.getParser(file.getPath());
		EventElement event;
		while ((event = parser.getEvent()) != null){
			for (int i=startSimNum; i<sims.length; i++)
				event.execute(sims[i]);
		}
		for (int i=startSimNum; i<sims.length; i++)
			print(sims[i].printResult());
		print("   "+ file.getName());
		print("\n");
	}
	
	boolean filter(String file){
		String[] files = {
				"n_1000_c_10_1k_recv_e1000.txt",
				"n_1000_c_10_1MB_recv_e1000.txt",
				"netperf_udp_stream_recv_1k_e1000_3t.txt",
				"netperf_udp_stream_recv_4k_e1000_3t.txt",
				"netperf_tcp_stream_recv_1k_e1000_3t.txt",
				"netperf_tcp_stream_recv_4k_e1000_3t.txt",
				"netperf_tcp_rr_recv_1k_e1000_3t.txt",
				"netperf_udp_rr_recv_1k_e1000_3t.txt",
				"rcv_tcp_e1000.txt",
				"rcv_udp_e1000.txt"
		};
		for (int i=0; i<files.length; i++){
			if (file.equals(files[i]))
			  return true;
		}
		return false;
	}
}
