package main;

import simulations.prefetchers.DistancePrefetching;
import visitor_pattern.IEventElementVisitor;

public class DistanceSuit extends PrefetchSuit {

	
	
	public DistanceSuit(boolean isZero, ParserType type) {
		super(isZero,type);
		
		header = "#   ----------------------------------------\n" +
				"#   Distance Prefetcher simulation\n" +
				"#   ----------------------------------------\n" +
				"#  running with different sizes of: Prefetch Buffer, TLB, Distance entries\n" +
				"#  those 3 sizes are written above each triple: accuracy efficiency hitRate\n" +
				"#  all distance entries are of size 2\n";
		confTitles = //new int[][]{{32,32,8},{32,32,32},{32,32,64},{32,32,128},{32,32,256},{32,32,512}};
		new int[][]{{4,4,256},{4,32,256},{32,32,256},{4,32,512},{16,32,512},{32,32,512}};
		linesTitles = new String[]{"size of Prefetch Buffer","size of IOTLB","size of Distance Table"};
	}

	@Override
	IEventElementVisitor[] create_sims() {
		
		IEventElementVisitor[] sims = new IEventElementVisitor[confTitles.length];
		for (int i=0; i<sims.length; i++)
			sims[i] = new DistancePrefetching(3,confTitles[i][0],confTitles[i][1],confTitles[i][2],2);		
		return sims;
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (!checkAndParseInOutParam(args))
			return;
		DistanceSuit simulation = new DistanceSuit(false,getParserType(args));
		
		simulation.run(args);	
	}

}

