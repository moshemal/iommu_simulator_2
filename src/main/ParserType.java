package main;

import parsers.DefaultParser;
import parsers.DmaParser;
import parsers.IParser;

public enum ParserType{
	
	DEFAULT
	{
		public IParser getParser(String filePath){
			return new DefaultParser(filePath);
		}
	},
	DMA
	{
		public IParser getParser(String filePath){
			return new DmaParser(filePath);
		}
	};
	public abstract IParser getParser(String filePath);
}