package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class PrefetchSuit extends Main{
	
	static int minimum_args = 3;
	boolean isZero = false;
	
	String[] metricTitles = {"totalAcc","coherent","regular","tlbC","tlbR", "PBc", "PBr", "accuracy"};
	int[][] confTitles;
	String[] linesTitles;
	
	public PrefetchSuit(boolean isZero, ParserType type) {
		this.isZero = isZero;
		this.parserType = type;
	}
			
	static ParserType getParserType(String[] args){
		if(args[2].equals("default"))
			return ParserType.DEFAULT;
		else return ParserType.DMA;
	}
	static boolean isZero (String[] args){
		if (args.length > minimum_args && args[3].equals("zero"))
			return true;
		return false;
	}
	
	static boolean checkAndParseInOutParam(String[] args){
		if (args.length < minimum_args){
			System.out.println("not enogh args\n did you choose the parser? (dma or default)\nusage: <input> <stdout or output> <parser type> <zero or nothing>");
			return false;
		}
		if (args[1].equals("stdout")){
			outputRootPath = null;
		}else{
			outputRootPath = new File(args[1]);
			try {
				output = new BufferedWriter(new FileWriter(outputRootPath));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
					
		inputRootPath = new File(args[0]);
		if(!inputRootPath.isDirectory()){
			System.out.println("input or dir is not valids");
			return false;
		}
		return true;
	}
	
	void printLine(){
		print("#");
		for (int i=startSimNum; i<(confTitles.length);i++)
			for (int j=0; j< 9 * metricTitles.length; j++)
				print("-");
		print("------------------------");
		print("\n");
	}
	
	private void printPreTitles(){
		
		for (int line=0; line<3; line++){
			print(String.format("#%8d",confTitles[startSimNum][line]));
			for (int i=1; i<metricTitles.length; i++)
				print(String.format("%9d",confTitles[startSimNum][line]));
			
			for (int sim=startSimNum+1; sim<confTitles.length; sim++){
				print(String.format("|%8d",confTitles[sim][line]));
				for (int i=1; i<metricTitles.length; i++)
					print(String.format("%9d",confTitles[sim][line]));
			}
			print("   " + linesTitles[line] + "\n");
		}
		
	}
	private void printMetricTitles(){
		
		printPreTitles();
		printLine();
		
		print(String.format("#%8s", metricTitles[0]));//,"PBhitC"));
		for (int j = 1; j < metricTitles.length; j++)
			print(String.format("%9s",metricTitles[j]));
		
		for (int i=startSimNum; i<confTitles.length-1; i++){
			print(String.format("|%8s", metricTitles[0]));//,"PBhitC"));
			for (int j = 1; j < metricTitles.length; j++)
				print(String.format("%9s",metricTitles[j]));
		}
		print("   fileName\n");
	}
	
	void printHeader(String[] args){
		for (int i=0;i<args.length;i++)
			print("# " + args[i] + " ");
		print("\n" + this.header);
		print("#  traces files from: " + args[0] + "\n");
		print("\n");
		printLine();
		printMetricTitles();
		printLine();
	}
	
	
	
	
	
//	public static void main(String[] args) {
//		String[] levelOne = {"client","server"};
//		String[] levelTwo = {"netperf","apache"};
//		
//
//		//runSimulation(".//..//traces_27_11//server//apache//n_1000_c_10_1MB_e1000_server.txt",new Basic(3,56));
//		//runSimulation(".//..//traces_27_11//server//apache//n_1000_c_10_1MB_e1000_server.txt",new LifoClusteredStatistic(3,false, 0.1));
//		
//		System.out.println(", , , ");
//		
//		for(int i=0; i<levelOne.length; i++){
//
//			for(int j=0; j<levelTwo.length; j++){
//				String root = ".//..//traces_27_11" + "//" + levelOne[i] +"//" + levelTwo[j];
//				
//				String[] traceFiles;
//				File dir = new File(root);
//				traceFiles = dir.list();
//				for (String file:traceFiles){
//					//System.out.println(root + "//" + file);
//					System.out.print(file + "\n");
//					//System.out.println("-----------------------------------------------------------\nfile: " + file);
//					//runSimulation(root + "//" + file,new Basic(3,56));CheckHints
//					//runSimulation(root + "//" + file,new FirstKMapped(3, false, 2));
//					//runSimulation(root + "//" + file,new FirstKMapped(3, false, 4));
//					//runSimulation(root + "//" + file,new FirstKMapped(3, false, 8));
//					//runSimulation(root + "//" + file,new FirstKMapped(3, false, 16));
//					//runSimulation(root + "//" + file,new FirstKMapped(3, false, 32));
//			
//			    	//runSimulation(root + "//" + file,new ClusteredStatistic(3,true, 0.1));
//					//runSimulation(root + "//" + file,new Basic(3,true));
//					//runSimulation(root + "//" + file,new OneShotFifo(3,true,0.1));
//					//runSimulation(root + "//" + file,new FifoClusteredStatistic(3,false, 0.1));
//					//runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,4,32,256));
//					//runSimulation(root + "//" + file,new RecencyBasedZeroPrefetch(3,512,32,512));
//					
//					//runSimulation(root + "//" + file,new DistancePrefetching(3,32,32,512,2));
//					//runSimulation(root + "//" + file,new MarkovChainPrefetch(3,4,4,256,2));
//					//runSimulation(root + "//" + file,new MarkovChainPrefetch(3,4,4,512,2));
//					//runSimulation(root + "//" + file,new MarkovChainPrefetch(3,4,8,512,2));
//					//runSimulation(root + "//" + file,new MarkovChainPrefetch(3,8,8,512,2));
//					
//					//runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,4,32,256));
//					//runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,8,32,512));
//					//runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,16,32,512));
//					//runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,32,32,512));
//					
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,32,32,256));
//					
//					//runSimulation(root + "//" + file,new DistancePrefetching(3,4,4,256,2));
//					//runSimulation(root + "//" + file,new DistancePrefetching(3,4,4,512,2));
//					//runSimulation(root + "//" + file,new DistancePrefetching(3,4,8,512,2));
//					//runSimulation(root + "//" + file,new DistancePrefetching(3,8,8,512,2));
//					System.out.println("");
//					//runSimulation(root + "//" + file,new DmaPure(3,4));
//					//runSimulation(root + "//" + file,new DmaPure(3,8));
//					//runSimulation(root + "//" + file,new DmaPure(3,16));
//					//runSimulation(root + "//" + file,new DmaPure(3,32));
//
//				}
//				
//
//			}
//
//		}   
//		/*
//		System.out.println(", , , ");
//		for(int i=0; i<levelOne.length; i++){
//
//			for(int j=0; j<levelTwo.length; j++){
//				String root = ".//..//traces_27_11" + "//" + levelOne[i] +"//" + levelTwo[j];
//				
//				String[] traceFiles;
//				File dir = new File(root);
//				traceFiles = dir.list();
//				for (String file:traceFiles){
//					//System.out.println(root + "//" + file);
//					System.out.print(file + ", ");
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,4,32,256));
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,8,32,256));
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,16,32,256));
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,32,32,256));
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,4,4,256));
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,4,8,256));
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,4,16,256));
//					runSimulation(root + "//" + file,new RecencyBasedPrefetch(3,4,32,256));
//					System.out.println("");
//
//				}
//				
//
//			}
//		}*/
//		
//		
//		
//		/*
//		String root = ".//..//traces_27_11" + "//" + levelOne[0] +"//" + levelTwo[0];
//		String[] traceFiles;
//		File dir = new File(root);
//		traceFiles = dir.list();
//		runSimulation(root + "//" + traceFiles[0],new BasicStatistic(3));*/
//	}
	
	

}
