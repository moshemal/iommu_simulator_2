package main;

import java.io.File;

import simulations.statistics.CountAddressesPeriod;
import visitor_pattern.IEventElementVisitor;

public class Statistics extends Main {

	public Statistics() {
		inputRootPath = new File("./../traces_2_10");
	}

	@Override
	IEventElementVisitor[] create_sims() {
		IEventElementVisitor[] sims = {
			new CountAddressesPeriod(3)	
		};
		return sims;
	}

	@Override
	void printHeader(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Statistics simulation = new Statistics();
		simulation.run(args);

	}

}
