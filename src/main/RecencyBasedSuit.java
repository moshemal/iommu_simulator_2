package main;


import simulations.prefetchers.RecencyBasedPrefetch;
import simulations.prefetchers.RecencyBasedZeroPrefetch;
import visitor_pattern.IEventElementVisitor;


public class RecencyBasedSuit extends PrefetchSuit {
	
	public RecencyBasedSuit(boolean isZero, ParserType type){
		super(isZero,type); 
		header = "#   ----------------------------------------\n" +
				"#   Recency Based Prefetcher simulation\n" +
				"#   ----------------------------------------\n" +
				"#  running with different sizes of: Prefetch Buffer, TLB, Recency Stack\n" +
				"#  those 3 sizes are written above each triple: accuracy efficiency hitRate\n";
		
		//confTitles = new int[][]{{32,32,8},{32,32,32},{32,32,64},{32,32,128},{32,32,256},{32,32,512}};
		confTitles = new int[][]{{4,4,8},{4,4,32},{4,4,64},{4,4,128},{4,4,256},{4,4,512}};
		linesTitles = new String[]{"size of Prefetch Buffer","size of IOTLB","size of Recency Table"};
	}

	
	/**
	 * @param args
	 * 
	 * 
	 */
	public static void main(String[] args) {
		if (!checkAndParseInOutParam(args))
			return;
		RecencyBasedSuit simulation = new RecencyBasedSuit(isZero(args),getParserType(args));
		
		simulation.print("#    isZero = " + isZero(args) + "\n");
		
		simulation.run(args);
	
	}

	IEventElementVisitor[] create_sims(){
		IEventElementVisitor[] sims = new IEventElementVisitor[confTitles.length];
		if (isZero){
			for (int i=0; i<sims.length; i++)
				sims[i] = new RecencyBasedZeroPrefetch(3,confTitles[i][0],confTitles[i][1],confTitles[i][2]);
		}
		else{
			for (int i=0; i<sims.length; i++)
				sims[i] = new RecencyBasedPrefetch(3,confTitles[i][0],confTitles[i][1],confTitles[i][2]);
		}
		
		return sims;
	}
	

}
