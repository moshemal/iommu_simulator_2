package main;

import java.io.IOException;

import simulations.prefetchers.RiommuPrefetcher;
import visitor_pattern.IEventElementVisitor;

public class RIommuSuit extends PrefetchSuit{

	public RIommuSuit(boolean isZero, ParserType type) {
		super(isZero, type);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (!checkAndParseInOutParam(args))
			return;
		RIommuSuit simulation = new RIommuSuit(false,getParserType(args));
		//int length = simulation.get_sims().length;
		
		
		simulation.runAllFiles(inputRootPath);
		if (output != null){
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	IEventElementVisitor[] create_sims() {
		IEventElementVisitor[] sims = {
			new RiommuPrefetcher(3)
		};
		return sims;
	}

}
