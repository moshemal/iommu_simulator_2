package simulations;




public class Address {
	public final long virtualAdd;
	public int[] indexes = null;
	public int numOfAccesses = 0;
	public boolean enable = true;
	
	public Address(long address){
		this.virtualAdd = address;
	}
	
	public void addAccess(int index){
		numOfAccesses++;
		if (indexes == null){
			indexes = new int[1];
			indexes[0] = index;
			return;
		}
		
		if(numOfAccesses > 5)
			return;
		
		int[] newArr = new int[indexes.length + 1];
		for (int i = 0; i < indexes.length; i++){
			newArr[i] = indexes[i];
		}
		newArr[indexes.length] = index;
		indexes = newArr;
	}
	public int maxIndexWhenAccessed(){
		if (indexes == null)
			return -1;
		int max = 0;
		for(int i=0; i<indexes.length; i++){
			if (indexes[i] > max)
				max = indexes[i];
		}
		return max;
	}
	
	@Override
	public boolean equals(Object other){
		if (((Address)other).virtualAdd == this.virtualAdd)
			return true;
		return false;
	}
}
