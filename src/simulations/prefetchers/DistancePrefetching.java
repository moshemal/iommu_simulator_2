package simulations.prefetchers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class DistancePrefetching extends Prefetcher {
	
	final int sizeOfEntry;
	LinkedList<Long> distancesList					= new LinkedList<Long>();	
	HashMap<Long,LinkedList<Long>> distanceDB	= new HashMap<Long, LinkedList<Long>>();
	
	Long lastPageMiss = null;
	Long lastDistance = null;
	
	public DistancePrefetching(int domainToExamine, int sizeOfPrefetchBuffer,
			int sizeOfTlb, int numberOfPEntries, int sizeOfEntry) {
		super(domainToExamine, sizeOfPrefetchBuffer, sizeOfTlb,
				numberOfPEntries);
		this.sizeOfEntry = sizeOfEntry;
	}

	@Override
	void accessAddress(Long virtualAddress) {
		totalAccesses++;
		
		if (mappedCoherents.containsKey(virtualAddress))
			coherentAccesses++;
		else
			regularAccesses++;
		
		//check in the tlb
		int index = tlb.indexOf(virtualAddress);
		if (index >= 0){
			tlbHits++;
			if (mappedCoherents.containsKey(virtualAddress))
				tlbCoherentHits++;
			tlb.remove(index);
			tlb.addFirst(virtualAddress); //LRU
			return;
		}
		
		//check in the PB
		index = pBuffer.indexOf(virtualAddress);
		if (index >= 0){
			pbHits++;
			if (mappedCoherents.containsKey(virtualAddress))
				pbCoherentHits++;
		}
		
		addLru(tlb,sizeOfTlb,virtualAddress);
		
		doCurrentMiss(virtualAddress);
		updateLastMiss(virtualAddress);
	}

	void updateLastMiss(Long virtualAddress) {
		if (lastPageMiss == null){
			lastPageMiss = virtualAddress;
			return;
		}
		if (lastDistance == null){
			lastDistance = lastPageMiss - virtualAddress;
			return;
		}
		long distance = lastPageMiss - virtualAddress;
		LinkedList<Long> list = distanceDB.get(lastDistance);
		addLru(list,sizeOfEntry,distance);
		lastPageMiss = virtualAddress;
		lastDistance = distance;
	}

	void doCurrentMiss(Long virtualAddress) {
		if (lastPageMiss==null || lastDistance == null)
			return;
		long distance = lastPageMiss - virtualAddress;
		LinkedList<Long> list = distanceDB.get(distance);
		if (list == null){
			distanceDB.put(distance, new LinkedList<Long>());
			distancesList.addFirst(distance);
			//if there is to much entries delete one
			if (distancesList.size() > numberOfPEntries){
				Long dToRemove = distancesList.removeLast();
				distanceDB.remove(dToRemove);
			}
			return;
		}
		//prefetch
		Iterator<Long> iter = list.iterator();
		while(iter.hasNext()){
			this.numOfPrefetched++;
			Long address = virtualAddress + iter.next();
			if (this.mappedAddress.containsKey(address))
				addLru(pBuffer,sizeOfPrefetchBuffer,address);
		}
		
		//update tags list for LRU
		distancesList.remove(distance);
		distancesList.addFirst(distance);
		
	}
}
