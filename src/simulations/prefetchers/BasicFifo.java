package simulations.prefetchers;

import java.util.HashMap;
import java.util.LinkedList;

import simulations.Address;
import visitor_pattern.*;

public class BasicFifo implements IEventElementVisitor {

	final int domainToExamine;
	final int sizeOfPrefetchBuffer;
	/*
	 * mapped addresses lists
	 * 0 bi
	 * 1 to
	 * 2 from
	 * 3 coherent
	 */
	HashMap<Integer,LinkedList<Address>> lists = new HashMap<Integer,LinkedList<Address>>();
	int[] maxSizeOfList;
	
	//Statistic Data
	int[] accesses;
	int[] hits;
	int[] maxSizeOfBuff = {0,0,0,0};
	int mappedAddr 			= 0;
	int unMappedAddr 		= 0;
	int coherentAccesses 	= 0;
	int totalAccesses		= 0;
	
	
	//Prefetching Date
	HashMap<Integer,LinkedList<Address>> prefetchBuffers = new HashMap<Integer,LinkedList<Address>>();
	
	public BasicFifo(int domainToExamine, int sizeOfPrefetchBuffer) {
		this.domainToExamine		= domainToExamine;
		this.sizeOfPrefetchBuffer	= sizeOfPrefetchBuffer;
		
		for (int i=0; i<4; i++){
			lists.put(i, new LinkedList<Address>());
			prefetchBuffers.put(i, new LinkedList<Address>());
		}
		
		maxSizeOfList	= new int[4];
		accesses		= new int[4];
		hits			= new int[4];
	}

	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(toDevice,event.virtualAddress);
		
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(fromDevice,event.virtualAddress);
	}
	
	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		mappedAddr++;
		checkDirection(event.direction);
		addMap(event.direction,event.virtualAddress);
	}

	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		unMappedAddr++;
		checkDirection(event.direction);
		removeMap(event.direction,event.virtualAddress);
		
		
	}

	@Override
	public void visit(MapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		mappedAddr++;
		
		addMap(coherent,event.virtualAddress);
	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		unMappedAddr++;
		removeMap(coherent,event.virtualAddress);
	}

	@Override
	public void visit(MapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(UnMapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(HintRxEvent event) {
		if (event.domain != domainToExamine)
			return;
		coherentAccesses--;
	}

	@Override
	public void visit(HintTxEvent event) {
		if (event.domain != domainToExamine)
			return;
		coherentAccesses--;
	}

	@Override
	public void visit(NopEvent event) {
	}

	@Override
	public String printResult() {
		String[] hitRates = calculateHitRate();
			
		String res = this.getClass().getName() + " simulation:" +
				"\ntotal accesses:\t\t" + totalAccesses + 
				"\ntotal mapped:\t\t" + mappedAddr + 
				"\ntotal unMapped:\t\t" + unMappedAddr +
				"\n\t\tbiDirection toDevice fromDevice Coherents" +
				"\naccesses by type:\t" + accesses[0] + "\t" + accesses[1] + "\t" + accesses[2] + "\t" + accesses[3] +
				"\nmax sizes:\t\t" + maxSizeOfList[0] + "\t" + maxSizeOfList[1] + "\t" + maxSizeOfList[2] + "\t" + maxSizeOfList[3] +
				"\nhits by type:\t\t" + hits[0] + "\t" + hits[1] + "\t" + hits[2] + "\t" + hits[3] +
				"\nhit rates:\t\t" + hitRates[0] + "\t" + hitRates[1] + "\t" + hitRates[2] + "\t" + hitRates[3] +
				"\nmax size of buffer\t" + maxSizeOfBuff[0] + "\t" + maxSizeOfBuff[1] + "\t" + maxSizeOfBuff[2] + "\t" + maxSizeOfBuff[3] +
				"\n\n\n";
		
				
		return res;
	}

	protected String[] calculateHitRate() {
		String[] hitRates = new String[4];
		for (int i=0; i<4; i++){
			if (accesses[i] != 0){
				 double part = (double)hits[i]*100/accesses[i];
				 hitRates[i] = String.format("%.2f", part);
			}
			else {
				hitRates[i] = "No";
			}
		}
		return hitRates;
	}
	
	
	void checkDirection(int direction){
		if (direction < 0 || direction > 2)
			throw new RuntimeException("unknown direction type");
	}
	
	void addMap(int listIndex, long virtualAddress){
		
		LinkedList<Address> list = lists.get(listIndex);
		
		list.addLast(new Address(virtualAddress));
		if (maxSizeOfList[listIndex] < list.size())
			maxSizeOfList[listIndex] = list.size();
		prefetch(listIndex);
	}
	
	void removeMap(int direction, long virtualAddress){
		LinkedList<Address> prefetchBuffer	= prefetchBuffers.get(direction);
		LinkedList<Address> mappedAddresses	= lists.get(direction);
		
		mappedAddresses.remove(new Address(virtualAddress));
		prefetchBuffer.remove(new Address(virtualAddress));
		
		prefetch(direction);
	}
	
	void prefetch(int direction){
		LinkedList<Address> prefetchBuffer	= prefetchBuffers.get(direction);
		LinkedList<Address> mappedAddresses	= lists.get(direction);
		
		while((prefetchBuffer.size() < sizeOfPrefetchBuffer) &&
				mappedAddresses.size() > 0){
			Address address = mappedAddresses.removeFirst();
			prefetchBuffer.addLast(address);
		}
		updateMaxBuff();
	}
	
	void accessAddress (int direction, long virtualAddress){
		totalAccesses++;
		
		int index =  lists.get(coherent).indexOf(new Address(virtualAddress));
		int index2=  prefetchBuffers.get(coherent).indexOf(new Address(virtualAddress));
		
		if(index >= 0 || index2 >= 0){
			coherentAccesses++;
			accesses[coherent]++;
			if (index2 >= 0)
				hits[coherent]++;
			return;
		}
		
		accesses[direction]++;
		LinkedList<Address> buffer	= prefetchBuffers.get(direction);
		index =	buffer.indexOf(new Address(virtualAddress));
		
		if (index < 0)
			return;
		hits[direction]++;
		if (index == sizeOfPrefetchBuffer){
			buffer.removeFirst();
			prefetch(direction);
		}
	}
	
	void updateMaxBuff(){
		for(int i=0; i<4; i++){
			int size = prefetchBuffers.get(i).size();
			if (size > maxSizeOfBuff[i])
				maxSizeOfBuff[i] = size;
		}
	}
	
}
