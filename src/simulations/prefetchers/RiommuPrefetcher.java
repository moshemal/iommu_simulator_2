/**
 * 
 */
package simulations.prefetchers;

import java.util.LinkedList;

import visitor_pattern.HintRxEvent;
import visitor_pattern.HintTxEvent;
import visitor_pattern.IEventElementVisitor;
import visitor_pattern.MapCoherentEvent;
import visitor_pattern.MapEvent;
import visitor_pattern.MapSgEvent;
import visitor_pattern.NopEvent;
import visitor_pattern.ReadEvent;
import visitor_pattern.UnMapCoherentEvent;
import visitor_pattern.UnMapEvent;
import visitor_pattern.UnMapSgEvent;
import visitor_pattern.WriteEvent;

/**
 * @author moshemal
 *
 */
public class RiommuPrefetcher implements IEventElementVisitor {

	
	final int domainToExamine;
	/*
	 * Bi = 0, Tx = 1 Rx = 2
	 */
	LinkedList<Long> Descriptors = new LinkedList<Long>();
	LinkedList<Long> Tx = new LinkedList<Long>();
	LinkedList<Long> Rx = new LinkedList<Long>();
	
	LinkedList<Long> txTlb = new LinkedList<Long>();
	LinkedList<Long> rxTlb = new LinkedList<Long>();
	
	//statistics
	int hits			= 0;
	int accesses		= 0;
	int prefetches		= 0;
	int invalidations	= 0;
	int unmaps			= 0;
	int unusedAddr		= 0;
	
	/**
	 * 
	 */
	public RiommuPrefetcher(int domainToExamine) {
		this.domainToExamine = domainToExamine;
	}

	
	
	void accessAddress(Long virtualAddress, LinkedList<Long> tlb, LinkedList<Long> pageTable){
		if (Descriptors.indexOf(virtualAddress) >= 0)
			return;
		accesses++;
		if(tlb.size() == 0){
			accesses++;
			accesses--;
		}
		Long current; 
		current = tlb.get(0);
		if (current.equals(virtualAddress)){
			hits++;
			return;
		}
		tlb.removeFirst();
		prefetch(tlb,pageTable);
		current = tlb.get(0);
		if (current.equals(virtualAddress)){
			hits++;
			return;
		}
	}
	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(event.virtualAddress,txTlb,Tx);		
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(event.virtualAddress,rxTlb,Rx);
	}

	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		int size;
		switch (event.direction){
			case 1:		size = Tx.size();
						Tx.addLast(event.virtualAddress);
						if (size < 2)
							prefetch(txTlb,Tx);
						break;
			case 2: 	size = Rx.size();
						Rx.addLast(event.virtualAddress);
						if (size < 2)
							prefetch(rxTlb,Rx);
						break;
			default:	throw new RuntimeException("invalid direction");
		}
	}

	/* (non-Javadoc)
	 * @see visitor_pattern.IEventElementVisitor#visit(visitor_pattern.UnMapEvent)
	 */
	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		unmaps++;
		LinkedList<Long> pageTable;
		LinkedList<Long> tlb;
		
		switch (event.direction){
			case 1:		pageTable = Tx;
						tlb = txTlb;
						break;
			case 2: 	pageTable = Rx;
						tlb = rxTlb;
						break;
			default:	throw new RuntimeException("invalid direction");
		}
		
		int index = pageTable.indexOf(event.virtualAddress);
		if(index >= 0){
			pageTable.remove(index);
			unusedAddr++;
			return;
		}
		index = tlb.indexOf(event.virtualAddress);
		if(index >= 0){
			tlb.remove(index);
			invalidations++;
			prefetch(tlb,pageTable);
		}
	}

	/* (non-Javadoc)
	 * @see visitor_pattern.IEventElementVisitor#visit(visitor_pattern.MapCoherentEvent)
	 */
	@Override
	public void visit(MapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		Descriptors.add(event.virtualAddress);
	}

	/* (non-Javadoc)
	 * @see visitor_pattern.IEventElementVisitor#visit(visitor_pattern.UnMapCoherentEvent)
	 */
	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		int index = Descriptors.indexOf(event.virtualAddress);
		Descriptors.remove(index);
	}

	/* (non-Javadoc)
	 * @see visitor_pattern.IEventElementVisitor#visit(visitor_pattern.MapSgEvent)
	 */
	@Override
	public void visit(MapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	/* (non-Javadoc)
	 * @see visitor_pattern.IEventElementVisitor#visit(visitor_pattern.UnMapSgEvent)
	 */
	@Override
	public void visit(UnMapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}
	
	@Override
	public void visit(HintRxEvent event) {
		return;
	}

	@Override
	public void visit(HintTxEvent event) {
		return;
	}

	@Override
	public void visit(NopEvent event) {
		return;
	}

	@Override
	public String printResult() {
		String res =  (hits*100)/accesses + "\t" + ((prefetches*100)/accesses) + "\t" + 
				(invalidations*100)/unmaps + "\t" + unusedAddr;

		return res;
	}
	
	void prefetch(LinkedList<Long> tlb, LinkedList<Long> pageTable){
		while(tlb.size() < 2 && pageTable.size() > 0){
			prefetches++;
			tlb.addLast(pageTable.removeFirst());
		}
	}

}
