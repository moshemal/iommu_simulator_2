package simulations.prefetchers;

public class MarkovChainZeroPrefetch extends MarkovChainPrefetch{

	public MarkovChainZeroPrefetch(int domainToExamine,
			int sizeOfPrefetchBuffer, int sizeOfTlb, int numberOfMarckovEntrys,
			int sizeOfMarckovEntry) {
		super(domainToExamine, sizeOfPrefetchBuffer, sizeOfTlb, numberOfMarckovEntrys,
				sizeOfMarckovEntry);
	}

	@Override
	void unMap(Long address){
		unMappedAddr++;
		mappedAddress.remove(address);
		tlb.remove(address);
		pBuffer.remove(address);
		this.tagsList.remove(address);
		this.marckovChain.remove(address);
		if (lastMiss!=null  && lastMiss.equals(address))
			lastMiss = null;
	}
}
