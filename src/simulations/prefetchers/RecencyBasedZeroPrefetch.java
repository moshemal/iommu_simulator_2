package simulations.prefetchers;

public class RecencyBasedZeroPrefetch extends RecencyBasedPrefetch {

	public RecencyBasedZeroPrefetch(int domainToExamine,
			int sizeOfPrefetchBuffer, int sizeOfTlb, int sizeOfStack) {
		super(domainToExamine, sizeOfPrefetchBuffer, sizeOfTlb, sizeOfStack);
	}
	
	@Override
	void unMap(Long address){
		unMappedAddr++;
		mappedAddress.remove(address);
		tlb.remove(address);
		pBuffer.remove(address);
		this.recencyStack.remove(address);
	}
}
