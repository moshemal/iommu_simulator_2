package simulations.prefetchers;


import java.util.LinkedList;


public class RecencyBasedPrefetch extends Prefetcher{

	
	LinkedList<Long> recencyStack		= new LinkedList<Long>();	
	
	public RecencyBasedPrefetch(int domainToExamine, int sizeOfPrefetchBuffer, int sizeOfTlb, int sizeOfStack) {
		super(domainToExamine, sizeOfPrefetchBuffer, sizeOfTlb, sizeOfStack);
	}
	
	void accessAddress(Long virtualAddress){
		totalAccesses++;
		
		if (mappedCoherents.containsKey(virtualAddress))
			coherentAccesses++;
		else
			regularAccesses++;
		
		//update recency
		int recency = recencyStack.indexOf(virtualAddress);
		if (recency >= 0)
			recencyStack.remove(recency);
		recencyStack.addFirst(virtualAddress);
		if(recencyStack.size() > numberOfPEntries)
			recencyStack.removeLast();
		
		//check in the tlb
		int index = tlb.indexOf(virtualAddress);
		if (index >= 0){
			tlbHits++;
			addLru(tlb,sizeOfTlb,virtualAddress);
			if (mappedCoherents.containsKey(virtualAddress))
				tlbCoherentHits++;
			return;
		}
		//check in the PB
		index = pBuffer.indexOf(virtualAddress);
		if (index >= 0){
			if (mappedCoherents.containsKey(virtualAddress))
				pbCoherentHits++;
			pbHits++;
		}
		
		addLru(tlb,sizeOfTlb,virtualAddress);
		
		//prefetch
		if (recency >= 0){ //in the real we should do recency >= sizeOfTlb
			this.numOfPrefetched+=3;
			Long addToP = recencyStack.get(recency);
			if (mappedAddress.containsKey(addToP))
				addLru(pBuffer,sizeOfPrefetchBuffer,addToP);
			addToP = recencyStack.get(recency-1);
			if (mappedAddress.containsKey(addToP))
				addLru(pBuffer,sizeOfPrefetchBuffer,addToP);
			if (recencyStack.size() > recency+1 ){
				addToP = recencyStack.get(recency+1);
				if (mappedAddress.containsKey(addToP))
					addLru(pBuffer,sizeOfPrefetchBuffer,addToP);
			}
		}
	}

}
