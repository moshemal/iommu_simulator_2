package simulations.prefetchers;

import java.util.LinkedList;

import simulations.Address;
import simulations.Basic;

public class FirstKMapped extends Basic {
	
	final int sizeOfPrefetchBuffer;
	int[] hits = new int[4];
	
	public FirstKMapped(int domainToExamine, boolean countMultipleAccessAsOne, 
						int sizeOfPrefetchBuffer) {
		super(domainToExamine, countMultipleAccessAsOne);
		this.sizeOfPrefetchBuffer = sizeOfPrefetchBuffer;
	}
	
	@Override
	protected
	void accessAddress(int direction, long vAddress){
		
		LinkedList<Address> coherentList = lists.get(coherent);
		int index = coherentList.indexOf(new Address(vAddress));
		totalAccesses++;
		Address address = null;
		if (index >= 0){
			address = coherentList.get(index);
			coherentAccesses++;
			if(index < sizeOfPrefetchBuffer)
				hits[coherent]++;
			
		}else{
			if (countMultipleAccessAsOne && (vAddress==lastAdressesAccessed[direction]) )
				return;
			lastAdressesAccessed[direction] = vAddress;
			nonCoherentAccesses++;
			
			for(int i=1; i<lists.size(); i++){
				LinkedList<Address> list = lists.get(i);
				index = list.indexOf(new Address(vAddress));
				if(index >=0 ){
					address = list.get(index);
					if(index < sizeOfPrefetchBuffer)
						hits[i]++;
					if(direction != i)
						throw new RuntimeException("not the same lists");
					break;
				}
			}
			
			
		}
		
		if(address == null)
			throw new RuntimeException("address is null");
		
		address.addAccess(index);
		
	}
	@Override
	public String printResult(){
		finalizeSim();
		String res = "";
		String[] hitRate = calculateHitRate();
		for (int i=0; i<hitRate.length; i++)
			res+= hitRate[i] + " ";
		return res;
	}
	
	protected String[] calculateHitRate() {
		String[] hitRates = new String[4];
		for (int i=0; i<4; i++){
			if (accesses[i] != 0){
				 double part = (double)hits[i]*100/accesses[i];
				 hitRates[i] = String.format("%.2f", part);
			}
			else {
				hitRates[i] = "No";
			}
		}
		return hitRates;
	}

}
