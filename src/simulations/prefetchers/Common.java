package simulations.prefetchers;

import java.util.HashMap;
import java.util.LinkedList;

import simulations.Address;
import visitor_pattern.*;


public class Common implements IEventElementVisitor {

	final int domainToExamine;
	final int sizeOfPrefetchBuffer;
	final int sizeOfCache;
	
	/*
	 * mapped addresses lists
	 * 0 bi
	 * 1 to device   (the device execute read) 
	 * 2 from device (the device execute write)
	 * 3 coherent
	 */
	HashMap<Integer,LinkedList<Address>> lists = new HashMap<Integer,LinkedList<Address>>();
	
	//Statistic Data
	int[] maxSizeOfList	= {0,0,0,0};
	int[] accesses		= {0,0,0,0};
	int[] hits			= {0,0,0,0};
	
	int mappedAddr 			= 0;
	int unMappedAddr 		= 0;
	int totalAccesses		= 0;
		
	
	
	public Common(int domainToExamine, int sizeOfPrefetchBuffer, int sizeOfcache) {
		
		this.domainToExamine		= domainToExamine;
		this.sizeOfPrefetchBuffer	= sizeOfPrefetchBuffer;
		this.sizeOfCache			= sizeOfcache;
		for (int i=0; i<4; i++){
			lists.put(i, new LinkedList<Address>());
		}
	}

	@Override
	public void visit(ReadEvent event) {
		
		if (event.domain != domainToExamine)
			return;
		accessAddress(toDevice,event.virtualAddress);
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(fromDevice,event.virtualAddress);
	}
	
	Address accessAddress(int direction, long virtualAddress) {
		LinkedList<Address> list = lists.get(coherent);
		int index =  list.indexOf(new Address(virtualAddress));
		
		if(index>=0){
			accesses[coherent]++;
			return list.get(index);
		}
		
		accesses[direction]++;
		list = lists.get(direction);
		index =  list.indexOf(new Address(virtualAddress));
		Address address = list.get(index);
		address.addAccess(index);
		return address;
	}

	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		mappedAddr++;
		checkDirection(event.direction);
		addMap(event.direction,event.virtualAddress);

	}

	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		unMappedAddr++;
		checkDirection(event.direction);
		removeMap(event.direction,event.virtualAddress);
	}

	@Override
	public void visit(MapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		mappedAddr++;
		
		addMap(coherent,event.virtualAddress);
	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		unMappedAddr++;
		removeMap(coherent,event.virtualAddress);
	}

	@Override
	public void visit(MapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(UnMapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(HintRxEvent event) {
		if (event.domain != domainToExamine)
			return;
	}

	@Override
	public void visit(HintTxEvent event) {
		if (event.domain != domainToExamine)
			return;
	}

	@Override
	public void visit(NopEvent event) {
	}

	@Override
	public String printResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	String[] calculateHitRate() {
		
		String[] hitRates = new String[4];
		for (int i=0; i<4; i++){
			if (accesses[i] != 0){
				 double part = (double)hits[i]*100/accesses[i];
				 hitRates[i] = String.format("%.2f", part);
			}
			else {
				hitRates[i] = "No";
			}
		}
		return hitRates;
	}

	String[] calculatePortion() {
		String[] allAccessesPortion = new String[4];
		for (int i=0; i<4; i++){
			 double part = (double)accesses[i]*100/totalAccesses;
			 allAccessesPortion[i] = String.format("%.2f", part);
		}
		return allAccessesPortion;
	}
	
	void checkDirection(int direction){
		if (direction < 0 || direction > 2)
			throw new RuntimeException("unknown direction type");
	}
	
	void addMap(int listIndex, long virtualAddress){
		
		LinkedList<Address> list = lists.get(listIndex);
		
		if(list.indexOf(new Address(virtualAddress)) >= 0){
			throw new RuntimeException(String.format("%x is mapped more then once",virtualAddress));
		}
		
		list.addLast(new Address(virtualAddress));
		if (maxSizeOfList[listIndex] < list.size())
			maxSizeOfList[listIndex] = list.size();
	}
	
	void removeMap(int direction, long virtualAddress){
		LinkedList<Address> mappedAddresses	= lists.get(direction);
		
		mappedAddresses.remove(new Address(virtualAddress));
		
	}
}
