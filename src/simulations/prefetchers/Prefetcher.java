package simulations.prefetchers;

import java.util.HashMap;
import java.util.LinkedList;

import visitor_pattern.*;

public abstract class Prefetcher implements IEventElementVisitor{

	final int domainToExamine;
	final int sizeOfPrefetchBuffer;
	final int sizeOfTlb;
	final int numberOfPEntries;
	
	HashMap<Long,Long> mappedAddress	= new HashMap<Long, Long>();
	HashMap<Long,Long> mappedCoherents	= new HashMap<Long, Long>();	
	LinkedList<Long> tlb				= new LinkedList<Long>();
	LinkedList<Long> pBuffer			= new LinkedList<Long>();
	LinkedList<Long> lastPrefetchedList	= null;
	//long lastAccessed;
	
	//Statistic Data
	int maxCoherents		= 0;
	int tlbHits				= 0;
	int tlbCoherentHits		= 0;
	int pbHits				= 0;
	int pbCoherentHits		= 0;
	int mappedAddr 			= 0;
	int unMappedAddr 		= 0;
	int totalAccesses		= 0;
	int coherentAccesses	= 0;
	int regularAccesses 	= 0;
	int numOfPrefetched		= 0;
	int numOfAccessesAfterMiss = 0;
	int coherentsMisses		= 0;
	
	
	public Prefetcher(int domainToExamine, int sizeOfPrefetchBuffer, int sizeOfTlb, int numberOfPEntries) {
		this.domainToExamine 		= domainToExamine;
		this.sizeOfPrefetchBuffer	= sizeOfPrefetchBuffer;
		this.sizeOfTlb				= sizeOfTlb;
		this.numberOfPEntries		= numberOfPEntries;
	}

	abstract void accessAddress(Long virtualAddress);
	
	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		/*if (lastAccessed == event.virtualAddress)
			return;
		lastAccessed = event.virtualAddress;
		accessAddress(event.virtualAddress);*/
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		/*if (lastAccessed == event.virtualAddress)
			return;
		lastAccessed = event.virtualAddress;*/
		accessAddress(event.virtualAddress);
	}

	void map (Long address){
		mappedAddr++;
		mappedAddress.put(address, address);
	}
	
	void unMap(Long address){
		unMappedAddr++;
		mappedAddress.remove(address);
		tlb.remove(address);
		pBuffer.remove(address);
		lastPrefetchedList = null;
	}
	
	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		map(event.virtualAddress);
	}

	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMap(event.virtualAddress);
	}

	@Override
	public void visit(MapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		map(event.virtualAddress);
		mappedCoherents.put(event.virtualAddress, event.virtualAddress);
		if (mappedCoherents.size() > maxCoherents)
			maxCoherents = mappedCoherents.size();
	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMap(event.virtualAddress);
		mappedCoherents.remove(event.virtualAddress);
	}

	@Override
	public void visit(MapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(UnMapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(HintRxEvent event) {
		if (event.domain != domainToExamine)
			return;
	}

	@Override
	public void visit(HintTxEvent event) {
		if (event.domain != domainToExamine)
			return;
	}

	@Override
	public void visit(NopEvent event) {
		return;
	}

	@Override
	public String printResult() {
		double tlbHitRate		 	= (double)(tlbHits-tlbCoherentHits)*100/totalAccesses;
		//double tlbHitRate		 	= (double)(tlbHits)*100/totalAccesses;
		double tlbCoherentHitRate	= (double)tlbCoherentHits*100/totalAccesses;
		
		double pbRegHitRate			= (double)(pbHits - pbCoherentHits)*100/totalAccesses;
		//double pbHitRate			= (double)(pbHits)*100/totalAccesses;
		
		double pbCoherentHitRate	= (double)pbCoherentHits*100/totalAccesses;
		
		double accuracy			= (double)pbHits*100/(totalAccesses - tlbHits);
		//double efficiency 		= (double)pbHits*100/numOfPrefetched;
		//double uselessHits		= (double)numOfAccessesAfterMiss*100/pbHits;
		//String res = String.format("%.2f %.2f %.2f", accuracy,efficiency,tlbHitRate);
		/*String res = "tlb hit rate:\t" + String.format("%.2f", tlbHitRate) +
					"\nPB hit rate:\t" + String.format("%.2f", pbHitRate) +
					"\ntotal hit rate:\t" + String.format("%.2f", pbHitRate + tlbHitRate) +
					"\ntotalAccesses:\t\t" + totalAccesses;*/
		//return res;
		//return String.format("%9d%9d%9d",totalAccesses,coherentAccesses,regularAccesses);
		return String.format("%9d%9d%9d%9d%9d%9d%9d%9.2f",totalAccesses,coherentAccesses,regularAccesses,
				tlbCoherentHits, tlbHits-tlbCoherentHits, pbCoherentHits, pbHits - pbCoherentHits, accuracy);
		//return String.format("%9.2f%9.2f%9.2f%9.2f%9.2f%9.2f", accuracy, tlbHitRate, tlbCoherentHitRate, pbRegHitRate , pbCoherentHitRate, tlbHitRate + tlbCoherentHitRate + pbRegHitRate + pbCoherentHitRate);
	}
	
	void addLru(LinkedList<Long> list, int size, Long address){
		if (list == null)
			return;
		int index = list.indexOf(address);
		if (index >= 0)
			list.remove(index);
		list.addFirst(address);
		if (list.size() > size)
			list.removeLast();
	}
	
}
