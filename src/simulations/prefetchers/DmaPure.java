package simulations.prefetchers;

import java.util.HashMap;
import java.util.LinkedList;

import visitor_pattern.IEventElementVisitor;
import visitor_pattern.HintRxEvent;
import visitor_pattern.HintTxEvent;
import visitor_pattern.MapCoherentEvent;
import visitor_pattern.MapEvent;
import visitor_pattern.MapSgEvent;
import visitor_pattern.NopEvent;
import visitor_pattern.ReadEvent;
import visitor_pattern.UnMapCoherentEvent;
import visitor_pattern.UnMapEvent;
import visitor_pattern.UnMapSgEvent;
import visitor_pattern.WriteEvent;

public class DmaPure implements IEventElementVisitor {

	final int domainToExamine;
	final int sizeOfPrefetchBuffer;
	
	HashMap<Integer,LinkedList<Long>> lists = new HashMap<Integer,LinkedList<Long>>();
	
	//Statistic Data
		int[] accesses			= {0,0,0,0};
		int[] hits				= {0,0,0,0};
		int mappedAddr 			= 0;
		int unMappedAddr 		= 0;
		int totalAccesses		= 0;
	
	public DmaPure(int domainToExamine, int sizeOfPrefetchBuffer) {
		this.domainToExamine		= domainToExamine;
		this.sizeOfPrefetchBuffer	= sizeOfPrefetchBuffer;
		
		for (int i=0; i<4; i++){
			lists.put(i, new LinkedList<Long>());
		}
	}

	void accessAddress (int direction, long virtualAddress){
		totalAccesses++;
		LinkedList<Long> list = lists.get(coherent);
		int index =  list.indexOf(virtualAddress);
		if(index >= 0){
			accesses[coherent]++;
			if (index<sizeOfPrefetchBuffer)
				hits[coherent]++;
			return;
		}
		
		list = lists.get(direction);
		index =  list.indexOf(virtualAddress);
		accesses[direction]++;
		
		if (index < 0)
			throw new RuntimeException("accessed unmapped address");
		
		if(index<sizeOfPrefetchBuffer){
			hits[direction]++;
			return;
		}
		
	}
	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(toDevice,event.virtualAddress);
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(fromDevice,event.virtualAddress);
	}

	@Override
	public void visit(MapEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(UnMapEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(MapCoherentEvent event) {
		
		if (event.domain != domainToExamine)
			return;
		lists.get(coherent).addFirst(event.virtualAddress);
	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		
		if (event.domain != domainToExamine)
			return;
		lists.get(coherent).remove(event.virtualAddress);
	}

	@Override
	public void visit(MapSgEvent event) {
		
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(UnMapSgEvent event) {
		
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");

	}

	@Override
	public void visit(HintRxEvent event) {
		
		if (event.domain != domainToExamine)
			return;
		accesses[coherent]--;
		hits[coherent]--;
		totalAccesses--;
		lists.put(fromDevice, event.addressList);
	}

	@Override
	public void visit(HintTxEvent event) {
		
		if (event.domain != domainToExamine)
			return;
		accesses[coherent]--;
		hits[coherent]--;
		totalAccesses--;
		lists.put(toDevice, event.addressList);
	}

	@Override
	public void visit(NopEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public String printResult() {
		String[] allAccessesPortion = new String[4];
		for (int i=0; i<4; i++){
			 double part = (double)accesses[i]*100/totalAccesses;
			 allAccessesPortion[i] = String.format("%.2f", part);
		}
		
		String[] hitRates = new String[4];
		for (int i=0; i<4; i++){
			if (accesses[i] != 0){
				 double part = (double)hits[i]*100/accesses[i];
				 hitRates[i] = String.format("%.2f", part);
			}
			else {
				hitRates[i] = "No";
			}
		}
		
		String res = "size of prefetch buffer: " + sizeOfPrefetchBuffer + 
				"\n\t\tbiDirection toDevice fromDevice Coherents" +
				"\nportion of accesses:\t" + allAccessesPortion[0] + "\t" + allAccessesPortion[1] + "\t" + allAccessesPortion[2] + "\t" + allAccessesPortion[3] +
				"\nhit rates:\t\t" + hitRates[0] + "\t" + hitRates[1] + "\t" + hitRates[2] + "\t" + hitRates[3] +
				"\n";
		
				/*String res = this.getClass().getName() + " simulation:" +
				"\ntotal accesses:\t\t" + totalAccesses + 
				"\ntotal mapped:\t\t" + mappedAddr + 
				"\ntotal unMapped:\t\t" + unMappedAddr +
				"\n\t\tbiDirection toDevice fromDevice Coherents" +
				"\naccesses by type:\t" + accesses[0] + "\t" + accesses[1] + "\t" + accesses[2] + "\t" + accesses[3] +
				
				"\nhits by type:\t\t" + hits[0] + "\t" + hits[1] + "\t" + hits[2] + "\t" + hits[3] +
				"\nhit rates:\t\t" + hitRates[0] + "\t" + hitRates[1] + "\t" + hitRates[2] + "\t" + hitRates[3] +
				
				"\n\n\n";
		*/
				
		return res;
	}

}
