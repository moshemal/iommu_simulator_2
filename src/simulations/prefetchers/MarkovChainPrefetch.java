package simulations.prefetchers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;


public class MarkovChainPrefetch extends Prefetcher {
	
	final int sizeOfMarckovEntry;
	LinkedList<Long> tagsList					= new LinkedList<Long>();	
	HashMap<Long,LinkedList<Long>> marckovChain	= new HashMap<Long, LinkedList<Long>>();
	
	Long lastMiss = null;
	
	public MarkovChainPrefetch(int domainToExamine, int sizeOfPrefetchBuffer, int sizeOfTlb, 
								 int numberOfMarckovEntrys, int sizeOfMarckovEntry) {
		
		super(domainToExamine, sizeOfPrefetchBuffer, sizeOfTlb, numberOfMarckovEntrys);
		this.sizeOfMarckovEntry		= sizeOfMarckovEntry;
	}

	@Override
	void accessAddress(Long virtualAddress) {
		totalAccesses++;
		if (mappedCoherents.containsKey(virtualAddress))
			coherentAccesses++;
		else
			regularAccesses++;
		//check in the tlb
		int index = tlb.indexOf(virtualAddress);
		if (index >= 0){
			tlbHits++;
			if (mappedCoherents.containsKey(virtualAddress))
				tlbCoherentHits++;
			tlb.remove(index);
			tlb.addFirst(virtualAddress); //LRU
			lastPrefetchedList = null;
			return;
		}
		if (this.mappedCoherents.containsKey(virtualAddress))
			this.coherentsMisses++;
		//check in the PB
		index = pBuffer.indexOf(virtualAddress);
		if (index >= 0){
			pbHits++;
			if (mappedCoherents.containsKey(virtualAddress))
				pbCoherentHits++;
		}
		
		if (lastPrefetchedList != null && lastPrefetchedList.indexOf(virtualAddress) >= 0)
			numOfAccessesAfterMiss++;
		lastPrefetchedList = null;
		
		addLru(tlb,sizeOfTlb,virtualAddress);
		
		doCurrentMiss(virtualAddress);
		updateLastMiss(virtualAddress);
		
	}
	
	void doCurrentMiss(Long virtualAddress){
		LinkedList<Long> list = marckovChain.get(virtualAddress);
		if (list == null){
			marckovChain.put(virtualAddress, new LinkedList<Long>());
			tagsList.addFirst(virtualAddress);
			//if there is to much entries delete one
			if (tagsList.size() > numberOfPEntries){
				Long address = tagsList.removeLast();
				marckovChain.remove(address);
			}
			return;
		}
		//prefetch
		lastPrefetchedList = new LinkedList<Long>();
		Iterator<Long> iter = list.iterator();
		while(iter.hasNext()){
			Long address = iter.next();
			this.numOfPrefetched++;
			if (!address.equals(virtualAddress) && this.mappedAddress.containsKey(address)){
				lastPrefetchedList.add(address);
				addLru(pBuffer,sizeOfPrefetchBuffer,address);
			}
		}
		
		//update tags list for LRU
		tagsList.remove(virtualAddress);
		tagsList.addFirst(virtualAddress);
	}
	
	void updateLastMiss(Long virtualAddress){
		if (lastMiss == null){
			lastMiss = virtualAddress;
			return;
		}
		LinkedList<Long> list = marckovChain.get(lastMiss);
		addLru(list,sizeOfMarckovEntry,virtualAddress);
		lastMiss = virtualAddress;
	}
	
	
}
