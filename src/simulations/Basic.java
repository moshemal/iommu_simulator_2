package simulations;

import java.util.HashMap;
import java.util.LinkedList;

import visitor_pattern.*;


public class Basic implements IEventElementVisitor {

	protected final int 		domainToExamine;
	protected final boolean 	countMultipleAccessAsOne;
	protected long[] 			lastAdressesAccessed;
	/*
	 * mapped addresses lists
	 * 0 bi
	 * 1 to
	 * 2 from
	 * 3 coherent
	 */
	protected HashMap<Integer,LinkedList<Address>> lists = new HashMap<Integer,LinkedList<Address>>();
	protected int[] maxSizeOfList;
	//ReadEvent lastReadEvent = null;
	
	
	/*
	 * statistic data 
	 */
	
	protected int[] accesses;
	protected int totalAccesses 		= 0;
	protected int mappedAddr 			= 0;
	protected int unMappedAddr 			= 0;
	protected int unaccessedAdd 		= 0;
	protected int coherentAccesses 		= 0;
	protected int nonCoherentAccesses	= 0;
	
	public Basic(int domainToExamine, boolean countMultipleAccessAsOne) {
		this.domainToExamine 			= domainToExamine;
		this.countMultipleAccessAsOne	= countMultipleAccessAsOne;
		
		for(int i=0; i<4; i++){
			lists.put(i, new LinkedList<Address>());
		}
		maxSizeOfList			= new int[4];
		accesses				= new int[4];
		lastAdressesAccessed	= new long[4];
	}

	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(toDevice,event.virtualAddress);
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(fromDevice,event.virtualAddress);

	}

	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		mappedAddr++;
		checkDirection(event.direction);
		addMap(event.direction,event.virtualAddress);

	}

	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMappedAddr++;
		checkDirection(event.direction);
		Address address = removeMap(event.direction,event.virtualAddress);
		
		accesses[event.direction]+= address.numOfAccesses;

	}

	@Override
	public void visit(MapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		mappedAddr++;
		
		addMap(coherent,event.virtualAddress);
	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMappedAddr++;
		Address address = removeMap(coherent,event.virtualAddress);
		accesses[coherent]+= address.numOfAccesses;
	}

	@Override
	public void visit(MapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(UnMapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");
	}

	@Override
	public void visit(HintRxEvent event) {
		if (event.domain != domainToExamine)
			return;
	}

	@Override
	public void visit(HintTxEvent event) {
		if (event.domain != domainToExamine)
			return;
	}

	@Override
	public void visit(NopEvent event) {
		return;
	}

	@Override
	public String printResult() {
		finalizeSim();
		return null;
	}
	
	protected 
	void finalizeSim(){
		Address add = null;
		LinkedList<Address> list;
		
		for (int i=0; i<4; i++){
			list = lists.get(i);
			while(list.size() > 0){
				add = list.removeFirst();
				accesses[i]+=add.numOfAccesses;
			}
		}
	}
	
	protected 
	void accessAddress(int direction, long vAddress){
		
		LinkedList<Address> coherentList = lists.get(coherent);
		int index = coherentList.indexOf(new Address(vAddress));
		
		Address address;
		if (index >= 0){
			address = coherentList.get(index);
			totalAccesses++;
			coherentAccesses++;
		}else{
			LinkedList<Address> list = lists.get(direction);
			if (countMultipleAccessAsOne && (vAddress==lastAdressesAccessed[direction]) )
				return;
			lastAdressesAccessed[direction] = vAddress;
			index = list.indexOf(new Address(vAddress));
			address = list.get(index);
			totalAccesses++;
			nonCoherentAccesses++;
		}
		
		address.addAccess(index);
	}
	
	protected 
	void checkDirection(int direction){
		if (direction < 0 || direction > 2)
			throw new RuntimeException("unknown direction type");
	}
	
	protected 
	void addMap(int listIndex, long virtualAddress){
		
		LinkedList<Address> list = lists.get(listIndex);
		
		list.addLast(new Address(virtualAddress));
		if (maxSizeOfList[listIndex] < list.size())
			maxSizeOfList[listIndex] = list.size();
	}
	
	protected 
	Address removeMap(int listIndex, long virtualAddress){
		Address address;
		LinkedList<Address> list = lists.get(listIndex);
		
		if (lastAdressesAccessed[listIndex] == virtualAddress)
			lastAdressesAccessed[listIndex] = 0;
		int index = list.indexOf(new Address(virtualAddress));
		address = list.remove(index);
		return address;
	}
}
