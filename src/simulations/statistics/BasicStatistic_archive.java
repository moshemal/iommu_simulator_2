package simulations.statistics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import simulations.Address;
import visitor_pattern.*;

public class BasicStatistic_archive implements IEventElementVisitor {

	final int 		domainToExamine;
	final boolean 	countMultipleAccessAsOne;
	double 			lastAdressAccessed;
	/*
	 * mapped addresses lists
	 * 0 bi
	 * 1 to
	 * 2 from
	 * 3 coherent
	 */
	HashMap<Integer,LinkedList<Address>> lists = new HashMap<Integer,LinkedList<Address>>();
	int[] maxSizeOfList;
	//ReadEvent lastReadEvent = null;
	
	
	/*
	 * statistic data 
	 */
	
	int[] accesses;
	int totalAccesses 		= 0;
	int mappedAddr 			= 0;
	int unMappedAddr 		= 0;
	int unaccessedAdd 		= 0;
	int coherentAccesses 	= 0;
	int nonCoherentAccesses = 0;
	
	/*
	 * table data
	 * 0 bi
	 * 1 to
	 * 2 from
	 * 3 coherent
	 */
	HashMap<Integer,HashMap<Integer,int[]>> tables = new HashMap<Integer,HashMap<Integer,int[]>>();
	final int maxNofA = 5;
	final int sizeOfRow = maxNofA + 2;
	
	//long lastAddress = 0;
	
	public BasicStatistic_archive(int domainToExamine, boolean countMultipleAccessAsOne) {
		this.domainToExamine 			= domainToExamine;
		this.countMultipleAccessAsOne 	= countMultipleAccessAsOne;
		
		for(int i=0; i<4; i++){
			lists.put(i, new LinkedList<Address>());
			tables.put(i, new HashMap<Integer,int[]>());
		}
		maxSizeOfList	= new int[4];
		accesses		= new int[4];
	}

	
	
	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		//lastReadEvent = event;
		accessAddress(lists.get(toDevice),event.virtualAddress);
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		accessAddress(lists.get(fromDevice),event.virtualAddress);
	}

	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		mappedAddr++;
		checkDirection(event.direction);
		addMap(event.direction,event.virtualAddress);
	}

	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMappedAddr++;
		checkDirection(event.direction);
		Address address = removeMap(event.direction,event.virtualAddress);
		
		accesses[event.direction]+= address.numOfAccesses;
		
		updateAddr(tables.get(event.direction), address);
	}

	@Override
	public void visit(MapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		mappedAddr++;
		
		addMap(coherent,event.virtualAddress);
	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMappedAddr++;
		Address address = removeMap(coherent,event.virtualAddress);
		accesses[coherent]+= address.numOfAccesses;
		
		updateAddr(tables.get(coherent), address);
	}
	
	@Override
	public void visit(MapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");

	}

	@Override
	public void visit(UnMapSgEvent event) {
		if (event.domain != domainToExamine)
			return;
		throw new RuntimeException("unimplemented method");

	}

	@Override
	public void visit(HintRxEvent event) {
		if (event.domain != domainToExamine)
			return;
		//lastReadEvent = null;
	}

	@Override
	public void visit(HintTxEvent event) {
		if (event.domain != domainToExamine)
			return;
		//lastReadEvent = null;
	}

	@Override
	public void visit(NopEvent event) {
		return;
	}
	
	void accessAddress(LinkedList<Address> list, long vAddress){
		
		LinkedList<Address> coherentList = lists.get(coherent);
		int index = coherentList.indexOf(new Address(vAddress));
		totalAccesses++;
		Address address;
		if (index >= 0){
			address = coherentList.get(index);
			coherentAccesses++;
		}else{
			if (countMultipleAccessAsOne && (vAddress==lastAdressAccessed) )
				return;
			lastAdressAccessed = vAddress;
			index = list.indexOf(new Address(vAddress));
			address = list.get(index);
			nonCoherentAccesses++;
		}
		
		address.addAccess(index);
	}
	
	@Override
	public String printResult() {
		
		String addingString = "last sizes of lists:\t" + lists.get(0).size() +
		"\t" + lists.get(1).size() + "\t" + lists.get(2).size() + "\t" + lists.get(3).size();
		finalizeSim();
		String res = this.getClass().getName() + " simulation:" +
				"\ntotal accesses:\t\t" + totalAccesses + 
				"\ntotal mapped:\t\t" + mappedAddr + 
				"\ntotal unMapped:\t\t" + unMappedAddr +
				"\nunaccessed addresses:\t" + unaccessedAdd +
				"\ncoherents accesses:\t" + coherentAccesses + 
				"\nnon coherent accesses:\t" + nonCoherentAccesses + 
				"\n\t\tbiDirection toDevice fromDevice Coherents" +
				"\naccesses by type:\t" + accesses[0] + "\t" + accesses[1] + "\t" + accesses[2] + "\t" + accesses[3] +
				"\nmax sizes:\t\t" + maxSizeOfList[0] + "\t" + maxSizeOfList[1] + "\t" + maxSizeOfList[2] + "\t" + maxSizeOfList[3] +
				"\n" + addingString +
				"\n";
		
		res+= printTable() + brLine + brLine + "\n\n";
		return res;
	}
	
	/*
	 * helpers methods
	 */
	void finalizeSim(){
		
		Address add = null;
		LinkedList<Address> list;
		
		for (int i=0; i<4; i++){
			list = lists.get(i);
			while(list.size() > 0){
				add = list.removeFirst();
				updateAddr(tables.get(i), add);
				accesses[i]+=add.numOfAccesses;
			}
		}
	}
	
	void addMap(int listIndex, long virtualAddress){
	
		LinkedList<Address> list = lists.get(listIndex);
		
		list.addFirst(new Address(virtualAddress));
		if (maxSizeOfList[listIndex] < list.size())
			maxSizeOfList[listIndex] = list.size();
	}
	
	void checkDirection(int direction){
		if (direction < 0 || direction > 2)
			throw new RuntimeException("unknown direction type");
	}
	
	Address removeMap(int listIndex, long virtualAddress){
		Address address;
		LinkedList<Address> list = lists.get(listIndex);
		
		if (lastAdressAccessed == virtualAddress)
			lastAdressAccessed = 0;
		int index = list.indexOf(new Address(virtualAddress));
		address = list.remove(index);
		return address;
	}
	
	/*
	 * printing methods and data
	 */
	
	final String brLine = "-------------------------------------------------------------------------\n";
	
	void sumRows(int[] base, int[] toAdd){
		if(toAdd == null)
			return;
		for (int i = 0; i < base.length; i++)
			base[i]+= toAdd[i];
	}
	
	String printTable(){
		String res = printHeader();
		
		int[] sumRow = new int[sizeOfRow];
		
		for (int i=0; i<tables.size(); i++){
			HashMap<Integer,int[]> table = tables.get(i);
			res+= "\t" + tablesNames[i] + "  # of rows: " + table.size() + "\n" + brLine;
			res+= printTable(table);
			res+= brLine;
			sumRows(sumRow,getSumOfTable(table)); 
		}
		
		res+=printRow(sumRow,"sum of all:");
		
		return res;
	}
	
	String printHeader(){
		String res = brLine + "| " + "\t\t|";
		for (int i = 1; i <= maxNofA ; i++)
			res+= "n(a)=" + i + " |";
		
		res+= "n(a)>=" + maxNofA + "| total |\n"; 
		res+= brLine;
		return res;
	}
	
	public void updateAddr(HashMap<Integer,int[]> table, Address add) {
		int dOfA = add.maxIndexWhenAccessed();
		int nOfA = add.numOfAccesses;
		
		if(dOfA < 0){
			if (nOfA == 0)
				unaccessedAdd++;
			else
				throw new RuntimeException("impossible nOfA and dOfA");
			return;
		}
			
		int[] row = table.get(dOfA);
		if(row == null){
			row = new int[sizeOfRow];
			table.put(dOfA, row);	
		}
		
		int j = nOfA;
		
		if (nOfA > maxNofA)
			j = sizeOfRow - 1;
		
		row[j] += add.numOfAccesses;
	}
		
	String printTable(HashMap<Integer,int[]> table){
		String res = "";
		
		
		Set<Integer> keys = table.keySet();
		TreeSet<Integer> tree = new TreeSet<Integer>(keys);
		Iterator<Integer> it = tree.iterator();
		
		int[] tableSum 		= new int[sizeOfRow];
		while(it.hasNext()){
			Integer key = it.next();
			int[] line = table.get(key);
			res+= printRow(line," " + key.toString() + "\t");
			sumRows(tableSum, line);
		}
		
		res+= printRow(tableSum,"sum: \t");
		return res;
	}
	
	String printRow(int[] row, String titleOfRow){
		if (row == null)
			return "";
		int sumOfRow = 0;
		
		String res = titleOfRow + "\t";
		for (int i = 1; i < row.length; i++){
			String value = printCell(row[i]);
			if (row[i] > 0){
				sumOfRow+= row[i];
			}
			res+= "|" + value + "\t"; 
		}
		res+= "|" + printCell(sumOfRow) + "\t|\n";
		return res;
	}
	
	String printCell(int cell){
		String res = "-";
		if (cell > 0){
			double part = (double)cell*100/(double)totalAccesses;
			res = String.format(" %.2f", part);
		}
		return res;
	}
	
	int[] getSumOfTable(HashMap<Integer,int[]> table){
		int[] result = new int[sizeOfRow];
		Iterator<int[]> iter = table.values().iterator();
		while(iter.hasNext()){
			int[] row = iter.next();
			sumRows(result, row);
		}
		return result;
	}
}
