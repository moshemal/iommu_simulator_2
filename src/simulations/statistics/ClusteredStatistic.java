 package simulations.statistics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class ClusteredStatistic extends BasicStatistic {

	final double threshold;
	public ClusteredStatistic(int domainToExamine, boolean countMultipleAccessAsOne, double threshold) {
		super(domainToExamine, countMultipleAccessAsOne);
		this.threshold = threshold;
	}
	
	boolean isBigThenThreshold(int[] arr){
		for(int i = 0; i < arr.length; i++){
			double part = (double)arr[i]*100/(double)totalAccesses;
			if (this.threshold < part)
				return true;
		}
		return false;
	}
	@Override
	protected
	String printTable(HashMap<Integer,int[]> table){
		String res = "";
		
		
		Set<Integer> keys = table.keySet();
		TreeSet<Integer> tree = new TreeSet<Integer>(keys);
		Iterator<Integer> it = tree.iterator();
		
		int[] clusteredLine 	= null;
		String clusteredTitle	= null;
		int[] tableSum 			= new int[sizeOfRow];
		
		while(it.hasNext()){
			Integer key = it.next();
			int[] line = table.get(key);
			sumRows(tableSum, line);
			
			if (isBigThenThreshold(line)){
				//did i clustered some lines before
				if (clusteredLine != null){
					clusteredTitle+=key.toString();
					res+= printRow(clusteredLine,clusteredTitle + "\t");
					clusteredLine	= null;
					clusteredTitle	= null;
				}
				res+= printRow(line," " + key.toString() + "\t");
			}else{
				//did i clustered some lines before
				if (clusteredLine == null){
					clusteredLine	= new int[sizeOfRow];
					clusteredTitle	= key.toString() + "-";
				}
				sumRows(clusteredLine,line);
			}
		}
		if (clusteredLine != null){
			clusteredTitle+=tree.last();
			res+= printRow(clusteredLine,clusteredTitle + "\t");
		}
		res+= printRow(tableSum,"sum: \t");
		return res;
	}
}
