package simulations.statistics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.HashSet;

import visitor_pattern.HintRxEvent;
import visitor_pattern.HintTxEvent;
import visitor_pattern.IEventElementVisitor;
import visitor_pattern.MapCoherentEvent;
import visitor_pattern.MapEvent;
import visitor_pattern.MapSgEvent;
import visitor_pattern.NopEvent;
import visitor_pattern.ReadEvent;
import visitor_pattern.UnMapCoherentEvent;
import visitor_pattern.UnMapEvent;
import visitor_pattern.UnMapSgEvent;
import visitor_pattern.WriteEvent;

public class CountAddressesPeriod implements IEventElementVisitor {
	
	final int domainToExamine;
	
	HashSet<Integer> periods = new HashSet<Integer>();
	HashSet<Integer> multipleMaps = new HashSet<Integer>();
	HashMap<Long,Integer> mapped = new HashMap<Long,Integer>();
	HashSet<Long> mappedCoherent = new HashSet<Long>();
	HashMap<Long,HashSet<Long>> firstAccessed = new HashMap<Long, HashSet<Long>>();
	long lastAccess = 0;
	
	public CountAddressesPeriod(int domain) {
		this.domainToExamine = domain;
	}

	
	void access(long address){
		if (lastAccess == address)
			return;
		lastAccess = address;
		
		//second+ access
		if (firstAccessed.containsKey(address)){
			HashSet<Long> addresses = firstAccessed.remove(address);
			periods.add(addresses.size());
		}/*else{
			if (mapped.remove(address)){//first access
				firstAccessed.put(address, new HashSet<Long>());
			}
		}*/
		
		Iterator<HashSet<Long>> iter = firstAccessed.values().iterator();
		while (iter.hasNext()){
			HashSet<Long> set = iter.next();
			set.add(address);
		}
		if (mapped.containsKey(address))
			firstAccessed.put(address, new HashSet<Long>());
	}
	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		access(event.virtualAddress);
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;
		
		access(event.virtualAddress);
	}

	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		Integer maps = mapped.get(event.virtualAddress);
		if (maps != null)
			maps++;
		else
			maps=0;
		mapped.put(event.virtualAddress, maps);
	}

	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		HashSet<Long> addresses = firstAccessed.remove(event.virtualAddress);
		if (addresses != null) periods.add(addresses.size());
		Integer maps = mapped.remove(event.virtualAddress);
		multipleMaps.add(maps);
	}

	@Override
	public void visit(MapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		mappedCoherent.add(event.virtualAddress);
	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;

	}

	@Override
	public void visit(MapSgEvent event) {
		if (event.domain != domainToExamine)
			return;

	}

	@Override
	public void visit(UnMapSgEvent event) {
		if (event.domain != domainToExamine)
			return;

	}

	@Override
	public void visit(HintRxEvent event) {
		if (event.domain != domainToExamine)
			return;

	}

	@Override
	public void visit(HintTxEvent event) {
		if (event.domain != domainToExamine)
			return;

	}

	@Override
	public void visit(NopEvent event) {
		return;
	}

	@Override
	public String printResult() {
		Iterator<Integer> iter = multipleMaps.iterator();
		String res = "";
		while (iter.hasNext()){
			res+=iter.next() + " ";
		}
		//res+="\n";
		return res;
	}

}
