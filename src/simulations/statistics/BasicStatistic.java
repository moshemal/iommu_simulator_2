package simulations.statistics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import simulations.Address;
import simulations.Basic;
import visitor_pattern.UnMapCoherentEvent;
import visitor_pattern.UnMapEvent;

public class BasicStatistic extends Basic {
	public
	final String brLine = "-------------------------------------------------------------------------\n";
	
	/*
	 * table data
	 * 0 bi
	 * 1 to
	 * 2 from
	 * 3 coherent
	 */
	HashMap<Integer,HashMap<Integer,int[]>> tables = new HashMap<Integer,HashMap<Integer,int[]>>();
	final int maxNofA = 5;
	final int sizeOfRow = maxNofA + 2;
	
	public BasicStatistic(int domainToExamine, boolean countMultipleAccessAsOne) {
		super(domainToExamine, countMultipleAccessAsOne);
		for(int i=0; i<4; i++){	
			tables.put(i, new HashMap<Integer,int[]>());
		}
	}
	
	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMappedAddr++;
		checkDirection(event.direction);
		Address address = removeMap(event.direction,event.virtualAddress);
		
		accesses[event.direction]+= address.numOfAccesses;
		
		updateAddr(tables.get(event.direction), address);
	}
	
	@Override
	public void visit(UnMapCoherentEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMappedAddr++;
		Address address = removeMap(coherent,event.virtualAddress);
		accesses[coherent]+= address.numOfAccesses;
		
		updateAddr(tables.get(coherent), address);
	}
	
	@Override
	public String printResult() {
		
		String addingString = "last sizes of lists:\t" + lists.get(0).size() +
		"\t" + lists.get(1).size() + "\t" + lists.get(2).size() + "\t" + lists.get(3).size();
		finalizeSim();
		String res = this.getClass().getName() + " simulation:" +
				"\ntotal accesses:\t\t" + totalAccesses + 
				"\ntotal mapped:\t\t" + mappedAddr + 
				"\ntotal unMapped:\t\t" + unMappedAddr +
				"\nunaccessed addresses:\t" + unaccessedAdd +
				"\ncoherents accesses:\t" + coherentAccesses + 
				"\nnon coherent accesses:\t" + nonCoherentAccesses + 
				"\n\t\tbiDirection toDevice fromDevice Coherents" +
				"\naccesses by type:\t" + accesses[0] + "\t" + accesses[1] + "\t" + accesses[2] + "\t" + accesses[3] +
				"\nmax sizes:\t\t" + maxSizeOfList[0] + "\t" + maxSizeOfList[1] + "\t" + maxSizeOfList[2] + "\t" + maxSizeOfList[3] +
				"\n" + addingString +
				"\n";
		
		//res+= printTable() + brLine + brLine + "\n\n";
		return res;
	}
	
	/*
	 * helpers methods
	 */
	@Override
	protected
	void finalizeSim(){
		
		Address add = null;
		LinkedList<Address> list;
		
		for (int i=0; i<4; i++){
			list = lists.get(i);
			while(list.size() > 0){
				add = list.removeFirst();
				updateAddr(tables.get(i), add);
				accesses[i]+=add.numOfAccesses;
			}
		}
	}
	
	protected 
	void updateAddr(HashMap<Integer,int[]> table, Address add) {
		int dOfA = add.maxIndexWhenAccessed();
		int nOfA = add.numOfAccesses;
		
		if(dOfA < 0){
			if (nOfA == 0)
				unaccessedAdd++;
			else
				throw new RuntimeException("impossible nOfA and dOfA");
			return;
		}
			
		int[] row = table.get(dOfA);
		if(row == null){
			row = new int[sizeOfRow];
			table.put(dOfA, row);	
		}
		
		int j = nOfA;
		
		if (nOfA > maxNofA)
			j = sizeOfRow - 1;
		
		row[j] += add.numOfAccesses;
	}
	
	protected
	void sumRows(int[] base, int[] toAdd){
		if(toAdd == null)
			return;
		for (int i = 0; i < base.length; i++)
			base[i]+= toAdd[i];
	}
	
	protected
	String printTable(){
		String res = printHeader();
		
		int[] sumRow = new int[sizeOfRow];
		
		for (int i=0; i<tables.size(); i++){
			HashMap<Integer,int[]> table = tables.get(i);
			res+= "\t" + tablesNames[i] + "  # of rows: " + table.size() + "\n" + brLine;
			res+= printTable(table);
			res+= brLine;
			sumRows(sumRow,getSumOfTable(table)); 
		}
		
		res+=printRow(sumRow,"sum of all:");
		
		return res;
	}
	
	protected
	String printHeader(){
		String res = brLine + "| " + "\t\t|";
		for (int i = 1; i <= maxNofA ; i++)
			res+= "n(a)=" + i + " |";
		
		res+= "n(a)>=" + maxNofA + "| total |\n"; 
		res+= brLine;
		return res;
	}
	
	protected
	String printTable(HashMap<Integer,int[]> table){
		String res = "";
		
		
		Set<Integer> keys = table.keySet();
		TreeSet<Integer> tree = new TreeSet<Integer>(keys);
		Iterator<Integer> it = tree.iterator();
		
		int[] tableSum 		= new int[sizeOfRow];
		while(it.hasNext()){
			Integer key = it.next();
			int[] line = table.get(key);
			res+= printRow(line," " + key.toString() + "\t");
			sumRows(tableSum, line);
		}
		
		res+= printRow(tableSum,"sum: \t");
		return res;
	}
	protected
	String printRow(int[] row, String titleOfRow){
		if (row == null)
			return "";
		int sumOfRow = 0;
		
		String res = titleOfRow + "\t";
		for (int i = 1; i < row.length; i++){
			String value = printCell(row[i]);
			if (row[i] > 0){
				sumOfRow+= row[i];
			}
			res+= "|" + value + "\t"; 
		}
		res+= "|" + printCell(sumOfRow) + "\t|\n";
		return res;
	}
	protected
	String printCell(int cell){
		String res = "-";
		if (cell > 0){
			double part = (double)cell*100/(double)totalAccesses;
			res = String.format(" %.2f", part);
		}
		return res;
	}
	protected
	int[] getSumOfTable(HashMap<Integer,int[]> table){
		int[] result = new int[sizeOfRow];
		Iterator<int[]> iter = table.values().iterator();
		while(iter.hasNext()){
			int[] row = iter.next();
			sumRows(result, row);
		}
		return result;
	}

}
