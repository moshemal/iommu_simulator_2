package simulations.statistics;

import java.util.LinkedList;

import simulations.Address;
import visitor_pattern.UnMapEvent;

public class OneShotFifo extends ClusteredStatistic {

	public OneShotFifo(int domainToExamine, boolean countMultipleAccessAsOne,
			double threshold) {
		super(domainToExamine, countMultipleAccessAsOne, threshold);
	}
	
	@Override
	protected 
	void accessAddress(int direction, long vAddress){
		
		LinkedList<Address> coherentList = lists.get(coherent);
		int index = coherentList.indexOf(new Address(vAddress));
		
		Address address;
		if (index >= 0){
			address = coherentList.get(index);
			totalAccesses++;
			coherentAccesses++;
		}else{
			LinkedList<Address> list = lists.get(direction);
			if (countMultipleAccessAsOne && (vAddress==lastAdressesAccessed[direction]) )
				return;
			
			address = removeMap(direction, lastAdressesAccessed[direction]);
			if(address != null){
				accesses[direction]+= address.numOfAccesses;
				updateAddr(tables.get(direction), address);
			}
			lastAdressesAccessed[direction] = vAddress;
			index = list.indexOf(new Address(vAddress));
			address = list.get(index);
			totalAccesses++;
			nonCoherentAccesses++;
		}
		
		address.addAccess(index);
	}
	
	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		unMappedAddr++;
		checkDirection(event.direction);
		Address address = removeMap(event.direction,event.virtualAddress);
		if (address == null)
			return;
		accesses[event.direction]+= address.numOfAccesses;
		
		updateAddr(tables.get(event.direction), address);
	}
	
	@Override
	protected 
	Address removeMap(int listIndex, long virtualAddress){
		Address address;
		LinkedList<Address> list = lists.get(listIndex);
		
		if (lastAdressesAccessed[listIndex] == virtualAddress)
			lastAdressesAccessed[listIndex] = 0;
		int index = list.indexOf(new Address(virtualAddress));
		if(index < 0)
			return null;
		address = list.remove(index);
		return address;
	}
}
