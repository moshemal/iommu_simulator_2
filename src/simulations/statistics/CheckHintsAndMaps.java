package simulations.statistics;

import java.util.HashMap;
import java.util.LinkedList;

import simulations.Address;
import visitor_pattern.IEventElementVisitor;
import visitor_pattern.Hint;
import visitor_pattern.HintRxEvent;
import visitor_pattern.HintTxEvent;
import visitor_pattern.MapCoherentEvent;
import visitor_pattern.MapEvent;
import visitor_pattern.MapSgEvent;
import visitor_pattern.NopEvent;
import visitor_pattern.ReadEvent;
import visitor_pattern.UnMapCoherentEvent;
import visitor_pattern.UnMapEvent;
import visitor_pattern.UnMapSgEvent;
import visitor_pattern.WriteEvent;

public class CheckHintsAndMaps implements IEventElementVisitor {

	protected final int domainToExamine;
	protected HashMap<Integer,LinkedList<Address>> lists = new HashMap<Integer,LinkedList<Address>>();
	
	public CheckHintsAndMaps(int domainToExamine ) {
		this.domainToExamine = domainToExamine;
		for(int i=0; i<4; i++){
			lists.put(i, new LinkedList<Address>());
		}
	}

	@Override
	public void visit(ReadEvent event) {
		if (event.domain != domainToExamine)
			return;
		
	}

	@Override
	public void visit(WriteEvent event) {
		if (event.domain != domainToExamine)
			return;

	}

	@Override
	public void visit(MapEvent event) {
		if (event.domain != domainToExamine)
			return;
		checkDirection(event.direction);
		addMap(event.direction,event.virtualAddress);
	}

	@Override
	public void visit(UnMapEvent event) {
		if (event.domain != domainToExamine)
			return;
		checkDirection(event.direction);
		removeMap(event.direction,event.virtualAddress);

	}

	@Override
	public void visit(MapCoherentEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(UnMapCoherentEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(MapSgEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(UnMapSgEvent event) {
		// TODO Auto-generated method stub

	}

	void check(Hint event){
		Long add = null;
		while (event.addressList.size() > 0){
			add = event.addressList.removeFirst();
			int index = event.addressList.indexOf(add);
			if (index >= 0)
				System.out.println(String.format("address: %lx is more then once at the list", add));
		}
	}
	@Override
	public void visit(HintRxEvent event) {
		check(event);
	}

	@Override
	public void visit(HintTxEvent event) {
		check(event);
	}

	@Override
	public void visit(NopEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public String printResult() {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected 
	void checkDirection(int direction){
		if (direction < 0 || direction > 2)
			throw new RuntimeException("unknown direction type");
	}
	
	protected 
	void addMap(int listIndex, long virtualAddress){
		
		LinkedList<Address> list = lists.get(listIndex);
		int index = list.indexOf(new Address(virtualAddress));
		if (index >= 0)
			System.out.println(String.format("mapping an existing virtual address: %x", virtualAddress));
		
		list.addLast(new Address(virtualAddress));
	}
	
	protected 
	Address removeMap(int listIndex, long virtualAddress){
		Address address;
		LinkedList<Address> list = lists.get(listIndex);
		
		int index = list.indexOf(new Address(virtualAddress));
		address = list.remove(index);
		
		return address;
	}
}
