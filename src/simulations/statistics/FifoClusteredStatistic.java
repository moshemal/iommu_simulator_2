package simulations.statistics;

import java.util.LinkedList;

import simulations.Address;

public class FifoClusteredStatistic extends ClusteredStatistic {

	public FifoClusteredStatistic(int domainToExamine, boolean countMultipleAccessAsOne, double threshold) {
		super(domainToExamine, countMultipleAccessAsOne, threshold);
	}
	
	@Override
	protected
	void addMap(int listIndex, long virtualAddress){
		LinkedList<Address> list = lists.get(listIndex);
		
		list.addFirst(new Address(virtualAddress));
		if (maxSizeOfList[listIndex] < list.size())
			maxSizeOfList[listIndex] = list.size();
	}

}
