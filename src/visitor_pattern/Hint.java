package visitor_pattern;

import java.util.LinkedList;

public abstract class Hint implements EventElement {

	final public int 				domain;
	final public LinkedList<Long>	addressList;
	
	public Hint(int domain, LinkedList<Long> addresses) {
		this.domain			= domain;
		this.addressList	= addresses;
	}
}
