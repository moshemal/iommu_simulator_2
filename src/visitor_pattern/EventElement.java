package visitor_pattern;

public interface EventElement {
	/*
	 * accept() in the original design pattern
	 * each Event have to provide execute().
	 */
	public void execute(IEventElementVisitor visitor); 
}
