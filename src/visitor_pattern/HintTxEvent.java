package visitor_pattern;

import java.util.LinkedList;

public class HintTxEvent extends Hint {

	public HintTxEvent(int domain, LinkedList<Long> addresses) {
		super(domain, addresses);
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);
	}

}
