package visitor_pattern;

public class MapCoherentEvent extends DirEvent {

	public MapCoherentEvent(int direction, int domain, long virtualAddress) {
		super(direction, domain, virtualAddress);
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);

	}

}
