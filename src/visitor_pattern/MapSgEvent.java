package visitor_pattern;

public class MapSgEvent extends DirEvent {

	public MapSgEvent(int direction, int domain, long virtualAddress) {
		super(direction, domain, virtualAddress);
	
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);

	}

}
