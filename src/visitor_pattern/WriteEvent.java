package visitor_pattern;

public class WriteEvent extends Event {

	public WriteEvent(int domain, long virtualAdd, long physicalAdd) {
		super(domain, virtualAdd, physicalAdd);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);
	}

}
