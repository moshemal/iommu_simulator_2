package visitor_pattern;

public abstract class DirEvent implements EventElement {

	final public int	direction;
	final public int	domain;
	final public long	virtualAddress;
	
	public DirEvent(int direction, int domain, long virtualAddress){
		this.direction		= direction;
		this.domain			= domain;
		this.virtualAddress	= mask12lsb(virtualAddress);
	}
	
	long mask12lsb(long add){
		long mask = 0xFFF;
		long result = (~mask) & add;
		return result;
	}
}
