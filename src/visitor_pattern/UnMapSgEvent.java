package visitor_pattern;

public class UnMapSgEvent extends DirEvent {

	public UnMapSgEvent(int direction, int domain, long virtualAddress) {
		super(direction, domain, virtualAddress);
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);
	}

}
