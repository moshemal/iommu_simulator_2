package visitor_pattern;

public class MapEvent extends DirEvent {

	public MapEvent(int direction, int domain, long virtualAddress) {
		super(direction, domain, virtualAddress);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);
	}
}
