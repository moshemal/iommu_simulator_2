package visitor_pattern;

public class UnMapCoherentEvent extends DirEvent {

	public UnMapCoherentEvent(int direction, int domain, long virtualAddress) {
		super(direction, domain, virtualAddress);
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);
	}

}
