package visitor_pattern;

public class NopEvent implements EventElement {

	public NopEvent() {
		// nothing to do
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		// nothing to do
	}
}
