package visitor_pattern;

public interface IEventElementVisitor {
	public final int biDirection	= 0;
	public final int toDevice		= 1;
	public final int fromDevice		= 2;
	public final int coherent		= 3;
	public final String[] 	tablesNames = {"B directional list","to device list","from device list","coherents list"}; 
	public void visit(ReadEvent event); 
	public void visit(WriteEvent event);
	public void visit(MapEvent event);
	public void visit(UnMapEvent event);
	public void visit(MapCoherentEvent event);
	public void visit(UnMapCoherentEvent event);
	public void visit(MapSgEvent event);
	public void visit(UnMapSgEvent event);
	public void visit(HintRxEvent event);
	public void visit(HintTxEvent event);
	public void visit(NopEvent event);
	public String printResult();
}
/*
 * 
  enum dma_data_direction {
          DMA_BIDIRECTIONAL = 0,
          DMA_TO_DEVICE = 1,
          DMA_FROM_DEVICE = 2,
          DMA_NONE = 3,
  };
 * 
 * 
 */
