package visitor_pattern;

public class ReadEvent extends Event {

	public ReadEvent(int domain, long virtualAdd, long physicalAdd) {
		super(domain, virtualAdd, physicalAdd);
		
	}

	@Override
	public void execute(IEventElementVisitor visitor) {
		visitor.visit(this);
	}
}
