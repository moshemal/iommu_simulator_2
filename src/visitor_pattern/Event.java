package visitor_pattern;

public abstract class Event implements EventElement{
	
	final public int domain;
	final public long virtualAddress;
	final public long physicalAddress;
	
	public Event(int domain, long virtualAdd, long physicalAdd) {
		this.domain 		= domain;
		this.virtualAddress		= mask12lsb(virtualAdd);
		this.physicalAddress	= mask12lsb(physicalAdd);
	}
	
	long mask12lsb(long add){
		long mask = 0xFFF;
		long result = (~mask) & add;
		return result;
	}

}
