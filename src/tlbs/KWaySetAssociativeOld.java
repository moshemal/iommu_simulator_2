package tlbs;

public class KWaySetAssociativeOld <T>{
	final int kPower;
	final int sizePower;
	final int numberOfWays;
	final int sizeOfWay;
	LruList<Tag,T>[] tlb;

	/**
	 * 
	 * @param kPower - the number of ways will be 2^kPower 
	 * @param sizePower - the size of the cache will be 2^sizePower
	 */
	public KWaySetAssociativeOld(int kPower, int sizePower) {
		if (sizePower < kPower)
			throw new RuntimeException("invalid parameters");
		this.kPower = kPower;
		this.sizePower = sizePower;
		this.sizeOfWay = 1 << (sizePower - kPower);
		numberOfWays = 1 << kPower;
		tlb = (LruList<Tag, T>[]) new LruList<?, ?>[sizeOfWay];
		for (int i = 0; i < sizeOfWay; i++)
			tlb[i] = new LruList<Tag,T>(numberOfWays);
	}

	public T getEntry(long virtualAdd, int did) {
		Tag tag = addressToTag(virtualAdd,did);
		return tlb[addressToSet(virtualAdd)].getElem(tag);
	}


	public T addEntry(long virtualAdd, int did, T entry) {
		Tag tag = addressToTag(virtualAdd,did);
		return tlb[addressToSet(virtualAdd)].addElem(tag, entry);
	}

	
	public void invalidateAll() {
		for (int i = 0; i < sizeOfWay; i++)
			tlb[i] = new LruList<Tag,T>(numberOfWays);
	}

	
	public void invalidate(long virtualAdd, int did) {
		Tag tag = addressToTag(virtualAdd,did);
		tlb[addressToSet(virtualAdd)].removeElem(tag);
	}
	
	public void invalidateDomain(int did){
		Tag tag = new Tag(0,did,true);
		for (int i = 0; i < sizeOfWay; i++){
			boolean isMore = true;
			while (isMore){
				isMore = (tlb[i].removeElem(tag) != null);
			}
		}
	}
			
	private Tag addressToTag(long address, int did){
		address = address >> 12;
		address = address >> (sizePower - kPower);
		return new Tag(address,did,false);
	}
	
	private int addressToSet(long address){
		long mask = sizeOfWay - 1;
		int res =(int) ((address >> 12) & mask);
		res = res % sizeOfWay;
		return res;
	}
}
