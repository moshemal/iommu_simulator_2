package tlbs;

import java.util.LinkedList;

public class SplitedDbuff<T> implements ITlb<T> {

	public final int size;
	public final int toEvacuate;
	LinkedList<Tag> dbuff;
	T entry = null;
	
	public SplitedDbuff(int size, int toEvacuate) {
		this.size = size;
		this.toEvacuate = toEvacuate;
		dbuff = new LinkedList<Tag>();
	}

	@Override
	public T getEntry(long virtualAdd, int did) {
		if (dbuff.contains(new Tag(virtualAdd,did,false)))
			return entry; //we don't care what is entry all we care is if it is hit
		return null;
	}

	@Override
	public T addEntry(long virtualAdd, int did, T entry) {
		this.entry = entry;
		if (dbuff.size() == size)
			evacuate();
		dbuff.addFirst(new Tag(virtualAdd,did,false));
		return null;
	}

	@Override
	public void invalidateAll() {
		dbuff = new LinkedList<Tag>();
	}

	@Override
	public void invalidate(long virtualAdd, int did) {
		dbuff.remove(new Tag(virtualAdd,did,false));	
	}

	@Override
	public void invalidateDomain(int did) {
		// TODO Auto-generated method stub
		
	}
	
	private void evacuate(){
		for (int i=0; i<toEvacuate; i++)
			dbuff.removeLast();
	}
}
