package tlbs;

public interface ITlb<T> {
	public T getEntry(long virtualAdd, int did);
	public T addEntry(long virtualAdd, int did, T entry);
	public void invalidateAll();
	public void invalidate(long virtualAdd, int did);
	public void invalidateDomain(int did);
}
