package tlbs;

public class KWaySetAssociative<T> implements ITlb<T>{
	
	final int numberOfWays;
	final int sizeOfWay;
	
	LruList<Tag,T>[] tlb;
	
	public KWaySetAssociative(int numberOfWays, int sizeOfWay) {
		
		this.sizeOfWay = sizeOfWay;
		this.numberOfWays = numberOfWays;
		tlb = (LruList<Tag, T>[]) new LruList<?, ?>[sizeOfWay];
		
		for (int i = 0; i < sizeOfWay; i++)
			tlb[i] = new LruList<Tag,T>(numberOfWays);
	}
	
	public T getEntry(long virtualAdd, int did) {
		Tag tag = addressToTag(virtualAdd,did);
		return tlb[addressToSet(virtualAdd)].getElem(tag);
	}


	public T addEntry(long virtualAdd, int did, T entry) {
		Tag tag = addressToTag(virtualAdd,did);
		return tlb[addressToSet(virtualAdd)].addElem(tag, entry);
	}

	
	public void invalidateAll() {
		for (int i = 0; i < sizeOfWay; i++)
			tlb[i] = new LruList<Tag,T>(numberOfWays);
	}

	
	public void invalidate(long virtualAdd, int did) {
		Tag tag = addressToTag(virtualAdd,did);
		tlb[addressToSet(virtualAdd)].removeElem(tag);
	}
	
	public void invalidateDomain(int did){
		Tag tag = new Tag(0,did,true);
		for (int i = 0; i < sizeOfWay; i++){
			boolean isMore = true;
			while (isMore){
				isMore = (tlb[i].removeElem(tag) != null);
			}
		}
	}
	
	public boolean justCheck(long virtualAdd, int did){
		Tag tag = addressToTag(virtualAdd,did);
		return tlb[addressToSet(virtualAdd)].justCheck(tag);
	}
	
	private Tag addressToTag(long address, int did){
		address = address >> 12;
		return new Tag(address,did,false);
	}
	
	private int addressToSet(long address){
		address = address >> 12;
		int res = (int)(address % sizeOfWay);
		return res;
	}
	
	protected class Tag{
		long page;
		int did;
		boolean isDomainInvalid = false;
		
		public Tag(long addr, int did, boolean isDomainInvalid){
			this.page = addr;
			this.did = did;
			this.isDomainInvalid = isDomainInvalid;
		}
		
		@Override
		public boolean equals(Object obj) {
		    if (this == obj)
		        return true;
		    if (obj == null)
		        return false;
		    if (getClass() != obj.getClass())
		        return false;
		    final Tag other = (Tag) obj;
		    if (this.did != other.did)
		        return false;
		    if(this.isDomainInvalid || other.isDomainInvalid)
		    	return true;
		    if(this.page != other.page)
		    	return false;
		    return true;
		}
	}
}
