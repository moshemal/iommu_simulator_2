package tlbs;



public class Tag {
	long page;
	int did;
	boolean isDomainInvalid = false;
	
	public Tag(long addr, int did, boolean isDomainInvalid){
		this.page = addr;
		this.did = did;
		this.isDomainInvalid = isDomainInvalid;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final Tag other = (Tag) obj;
	    if (this.did != other.did)
	        return false;
	    if(this.isDomainInvalid || other.isDomainInvalid)
	    	return true;
	    if(this.page != other.page)
	    	return false;
	    return true;
	}
}
