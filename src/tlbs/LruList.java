package tlbs;

import java.util.Iterator;
import java.util.LinkedList;

public class LruList<T,D> {

	private LinkedList<Element> set = null;
	private int size;
	
	public LruList(int size) {
		if (size < 0)
			throw new RuntimeException("size can't be negative");
		set = new LinkedList<Element>();
		this.size = size;
	}
	
	public D addElem(T tag, D data){
		Element result = null;
		Element entry = new Element(tag,data);
		set.addFirst(entry);
		if (set.size() > size){
			result = set.removeLast();
			return result.data;
		}
		return null;
	}
	
	public D getElem(T tag){
		Element elem = removeElemHndl(tag);
		if (elem == null)
			return null;
		set.addFirst(elem);
		return elem.data;
	}
	
	public D removeElem(T tag){
		Element elem = removeElemHndl(tag);
		if (elem == null)
			return null;
		return elem.data;
	}
	
	private Element removeElemHndl(T tag){
		Iterator<Element> iter = set.iterator();
		while(iter.hasNext()){
			Element elem = iter.next();
			if (elem.tag.equals(tag)){
				set.remove(elem);
				return elem;
			}
		}
		return null;
	}
	
	public boolean justCheck(T tag){
		Iterator<Element> iter = set.iterator();
		while(iter.hasNext()){
			Element elem = iter.next();
			if (elem.tag.equals(tag)){
				return true;
			}
		}
		return false;
	}
	
	class Element{
		public T tag;
		public D data;
		Element(T t, D d){
			tag = t;
			data = d;
		}
	}
}
