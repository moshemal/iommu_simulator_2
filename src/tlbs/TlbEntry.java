package tlbs;

class TlbEntry {
	
	public final long virtualAdd;
	public long physicalAdd;
	private long address;
	private int did; // domain id
	private long tag;
	
	private boolean translated;
	private boolean valid = false;
	private int hits;
	private int counter;

	TlbEntry(long vAddress, int did, long tag){
		this.virtualAdd = vAddress;
		this.did = did;
		this.tag = tag;
	}
	public long getAddress(){
		return this.address;
	}
	public void incHits() {
		hits++;
	}

	public int getHits() {
		return hits;
	}

	public void resetCounter() {
		counter = 0;
	}

	public void setTranslated(boolean aTranslated) {
		translated = aTranslated;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void incCounter(int n) {
		counter += n;
	}

	public int getDid() {
		return did;
	}

	public int getCounter() {
		return counter;
	}

	public long getTag() {
		return tag;
	}

	public boolean isTranslated() {
		return translated;
	}

	public boolean isValid() {
		return valid;
	}

}
