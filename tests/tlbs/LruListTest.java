package tlbs;

import junit.framework.Assert;

import org.junit.Test;

public class LruListTest {

	@Test
	public void test() {
		LruList<Integer,Integer> lru = new LruList<Integer, Integer>(1);
		Assert.assertEquals(null, lru.getElem(0));
		lru.addElem(1, 1);
		Assert.assertEquals(1, lru.getElem(1).intValue());
		lru.addElem(2, 2);
		Assert.assertEquals(null, lru.getElem(1));
		Assert.assertEquals(2, lru.getElem(2).intValue());
		
	}
	
	@Test
	public void test2(){
		LruList<Integer,Integer> lru = new LruList<Integer, Integer>(2);
		lru.addElem(1, 1);
		lru.addElem(2, 2);
		Assert.assertEquals(1,lru.getElem(1).intValue());
		lru.addElem(3, 3);
		Assert.assertEquals(null,lru.getElem(2));
		Assert.assertEquals(3,lru.getElem(3).intValue());
		lru.removeElem(3);
		lru.removeElem(1);
		Assert.assertEquals(null,lru.getElem(3));
		Assert.assertEquals(null,lru.getElem(1));
	}
}
