package tlbs;

import org.junit.Assert;
import org.junit.Test;

public class KWaySetAssociativeTest {

	@Test
	public void test1Way1Size() {
		KWaySetAssociative<Integer> cache = new KWaySetAssociative<Integer>(1,1); //1 way size = 1
		cache.addEntry(1 << 12, 0, 1);
		Assert.assertEquals(1, cache.getEntry(1 << 12, 0).intValue());
		cache.addEntry(2 << 12, 0, 2);
		Assert.assertEquals(2, cache.getEntry(2 << 12, 0).intValue());
		Assert.assertEquals(null, cache.getEntry(1 << 12, 0));
		cache.invalidate(2<<12, 0);
		Assert.assertEquals(null, cache.getEntry(2 << 12, 0));
		
		cache.addEntry(2 << 12, 0 ,2);
		Assert.assertEquals(2, cache.getEntry(2 << 12, 0).intValue());
		cache.invalidateAll();
		Assert.assertEquals(null, cache.getEntry(2 << 12, 0));
	}

	@Test
	public void test1Way2Size() {
		KWaySetAssociative<Integer> cache = new KWaySetAssociative<Integer>(1,2); //size = 2
		test2sizeFully(cache);
	}

	@Test
	public void test2Way2Size() {
		KWaySetAssociative<Integer> cache = new KWaySetAssociative<Integer>(2,1); //size = 2
		testXSize(cache,2);
	}
	
	@Test
	public void test2Way4Size() {
		KWaySetAssociative<Integer> cache = new KWaySetAssociative<Integer>(2,2); //size = 4
		testXSize(cache,4);
		testXSizeDomainInvalidate(cache,4);
	}
	
	@Test
	public void test2Way8Size() {
		KWaySetAssociative<Integer> cache = new KWaySetAssociative<Integer>(2,4); //size = 8
		testXSize(cache,8);
		testXSizeDomainInvalidate(cache,8);
	}
	
	@Test
	public void test4Way8Size() {
		KWaySetAssociative<Integer> cache = new KWaySetAssociative<Integer>(4,2); //size = 8
		testXSize(cache,8);
		testXSizeDomainInvalidate(cache,8);
	}
	
	private void test2sizeFully(KWaySetAssociative<Integer> cache){
		cache.addEntry(1 << 12, 0,1);
		cache.addEntry(2 << 12, 0,2);
		Integer elem = cache.addEntry(3 << 12, 0,3);
		Assert.assertEquals(null, cache.getEntry(1 << 12, 0));
		Assert.assertEquals(1,elem.intValue());
		Assert.assertEquals(3, cache.getEntry(3 << 12, 0).intValue());
		Assert.assertEquals(2, cache.getEntry(2 << 12, 0).intValue());
		cache.addEntry(1 << 12, 0,1);
		Assert.assertEquals(null, cache.getEntry(3 << 12, 0));
		Assert.assertEquals(2, cache.getEntry(2 << 12, 0).intValue());
		Assert.assertEquals(1, cache.getEntry(1 << 12, 0).intValue());
		cache.invalidate(2<<12, 0);
		Assert.assertEquals(null, cache.getEntry(2 << 12, 0));
		cache.invalidateAll();
		Assert.assertEquals(null, cache.getEntry(2 << 12, 0));
		Assert.assertEquals(null, cache.getEntry(1 << 12, 0));
	}
	
	private void testXSize(KWaySetAssociative<Integer> cache, int size){
		for(int i = 1; i <= size+1 ; i++)
			cache.addEntry(i << 12, 0, i);
		
		Assert.assertEquals(null, cache.getEntry(1 << 12, 0));
		for(int i = size+1; i > 1; --i)
			Assert.assertEquals(i, cache.getEntry(i << 12, 0).intValue());
		
		cache.addEntry(1 << 12, 0,1);
		Assert.assertEquals(null, cache.getEntry(size+1 << 12, 0));
		for(int i = size; i >= 1; --i)
			Assert.assertEquals(i, cache.getEntry(i << 12, 0).intValue());
		
		cache.invalidate(2<<12, 0);
		Assert.assertEquals(null, cache.getEntry(2 << 12, 0));
		
		cache.invalidateAll();
		for(int i = 1; i <= size; i++)
			Assert.assertEquals(null, cache.getEntry(i << 12, 0));
	}
	
	private void testXSizeDomainInvalidate(KWaySetAssociative<Integer> cache, int size){
		int i;
		for(i = 1; i <= size/2 ; i++)
			cache.addEntry(i << 12, 0, i);
		for(; i<=size; i++)
			cache.addEntry(i << 12, 1, i);
		
		for(i = 1; i <= size/2 ; i++)
			Assert.assertEquals(i, cache.getEntry(i << 12, 0).intValue());
		for(; i<=size; i++)
			Assert.assertEquals(i, cache.getEntry(i << 12, 1).intValue());
		cache.invalidateDomain(0);
		for(i = 1; i <= size/2 ; i++)
			Assert.assertEquals(null, cache.getEntry(i << 12, 0));
		for(; i<=size; i++)
			Assert.assertEquals(i, cache.getEntry(i << 12, 1).intValue());
	}
}
