package simulations.statistics;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import visitor_pattern.EventElement;
import visitor_pattern.IEventElementVisitor;
import visitor_pattern.MapEvent;
import visitor_pattern.ReadEvent;

public class CountAddressesPeriodTest{
	EventElement[] events;
	
	@Test
	public void test() {
		IEventElementVisitor sim = new CountAddressesPeriod(3);
		assertEquals("", sim.printResult());
		sim.visit(new MapEvent(0,3,0x10000));
		sim.visit(new MapEvent(0,3,0x20000));
		sim.visit(new ReadEvent(3,0x10000, 0));
		sim.visit(new ReadEvent(3,0x20000, 0));
		sim.visit(new ReadEvent(3,0x10000, 0));
		assertEquals("2 ", sim.printResult());
	}
}
